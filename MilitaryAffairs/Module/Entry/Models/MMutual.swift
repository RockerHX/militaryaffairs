//
//  MMutual.swift
//  News
//
//  Created by RockerHX on 2019/1/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Foundation


class MMutual: Codable {

    enum Mode: String, Codable {
        case native = "NATIVE"
        case store = "STORE"
        case web = "WEBVIEW"
    }

    let id: Int
    let title: String
    let summary: String
    let mode: Mode
    let url: String
    let status: String
    let flag: Int

}

