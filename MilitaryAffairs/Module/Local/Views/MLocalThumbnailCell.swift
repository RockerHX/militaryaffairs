//
//  MLocalThumbnailCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Kingfisher


class MLocalThumbnailCell: MLocalCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var thumbnailView: UIImageView?
    
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnailView?.kf.indicatorType = .activity
    }

    // MARK: - Public Methods -
    override func display(with local: MLocal) {
        super.display(with: local)
        if let url = local.thumbnails?.first {
            thumbnailView?.kf.setImage(with: URL(string: url))
        }
    }

    // MARK: - Private Methods -

}

