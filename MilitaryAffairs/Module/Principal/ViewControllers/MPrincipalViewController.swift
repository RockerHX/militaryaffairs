//
//  MPrincipalViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


enum NPrincipalSegue: String, HXSegue {

    typealias Element = UIViewController

    case Principal2Location = "Principal2Location"
    case Principal2Channel  = "Principal2Channel"
    case List2Detial        = "List2Detial"
}



extension Notification.Name {
    static let ConfigureOperationNotification = Notification.Name("ConfigureOperationNotification")
}



class MPrincipalViewController: UIViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var locationButton: UIButton?

    // MARK: - Public Property -
    // MARK: - Private Property -
    private var pagesViewController: PagesViewController?

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !LaunchManager.manager.channelsComfirmed {
            NPrincipalSegue.Principal2Channel.performSegue(on: self)
        }
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if NPrincipalSegue.Principal2Location.identifier == identifier {
                let locationViewController = (segue.destination as? UINavigationController)?.viewControllers.first as? MLocationViewController
                guard let snapshotView = view.snapshotView(afterScreenUpdates: true) else { return }
                locationViewController?.snapshotView = snapshotView
            } else if NPrincipalSegue.Principal2Channel.identifier == identifier {
                let channelViewController = (segue.destination as? UINavigationController)?.viewControllers.first as? MChannelViewController
                channelViewController?.delegate = self
            } else if String(describing: PagesViewController.self) == identifier {
                pagesViewController = segue.destination as? PagesViewController
            }
        }
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayLocationState), name: .ConfigureOperationNotification, object: nil)
        fetchAvailableChannels()
    }

    @objc private func displayLocationState() {
        let configuration = MAppConfigurationManager.manager.configuration
        guard let state = configuration?.locationState else { return }
        switch state {
        case .hidden:
            locationButton?.isHidden = true
        default:
            locationButton?.isHidden = false
        }
    }

    private func fetchAvailableChannels() {
        if let channels = MDChannel.fetchAvailableDressChannels() {
            var titles = [String]()
            var viewControllers = [UIViewController]()
            channels.forEach({ (channel) in
                titles.append(channel.name!)

                let controller = MPNewsListViewController.instance()
                controller.id = channel.id
                viewControllers.append(controller)
            })
            pagesViewController?.titles = titles
            pagesViewController?.viewControllers = viewControllers
            pagesViewController?.reloadData()
        }
    }
    
    private func channelsReload() {
        fetchAvailableChannels()
    }
    
}


// MARK: - MChannelViewControllerDelegate -
extension MPrincipalViewController: MChannelViewControllerDelegate {

    func channelsComfirm() {
        channelsReload()
    }
}

