//
//  MVideoRecommendCell.swift
//  News
//
//  Created by RockerHX on 2018/3/20.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Kingfisher


class MVideoRecommendCell: UITableViewCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var coverView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var durationLabel: UILabel?
    @IBOutlet var summaryLabel: UILabel?
    
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coverView?.kf.indicatorType = .activity
    }

    // MARK: - Public Methods -
    public func dispay(withVideo video: MVideo) {
        if let coverURL = video.coverURL {
            coverView?.kf.setImage(with: URL(string: coverURL))
        }
        titleLabel?.text = video.title
        durationLabel?.text = video.duration
        summaryLabel?.text = video.summary
    }

    // MARK: - Private Methods -

}

