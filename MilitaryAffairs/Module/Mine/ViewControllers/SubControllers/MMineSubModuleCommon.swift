//
//  MMineSubModuleCommon.swift
//  News
//
//  Created by RockerHX on 2018/3/20.
//  Copyright © 2018年 RockerHX. All rights reserved.
//

import Foundation


enum ModuleFlag: Int {
    case favorite
    case read
}


protocol MMineSubModuleCommon {

    func fetchFlagObjects<T, D>(caches: [D], flag: ModuleFlag) -> [T] where T: Codable, D: MDObjectType

}

extension MMineSubModuleCommon {

    func fetchFlagObjects<T, D>(caches: [D], flag: ModuleFlag) -> [T] where T: Codable, D: MDObjectType {
        var collection = [T]()
        let sortedResult = caches.sorted(by: { (first, last) -> Bool in
            switch flag {
            case .favorite:
                if let firstDate = first.favoriteDate,
                    let lastDate = last.favoriteDate {
                    return firstDate > lastDate
                }
            case .read:
                if let firstDate = first.readDate,
                    let lastDate = last.readDate {
                    return firstDate > lastDate
                }
            }
            return false
        })
        sortedResult.forEach({ (cache) in
            if let data = cache.data {
                if let object = try? JSONDecoder().decode(T.self, from: data) {
                    collection.append(object)
                }
            }
        })
        return collection
    }
}

