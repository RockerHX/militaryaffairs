//
//  MLocalListViewController.swift
//  DressBeautiful
//
//  Created by RockerHX on 2018/6/22.
//Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import PromiseKit
import CRRefresh
import RxSwift
import CoreLocation


class MLocalListViewController: UITableViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var coordinate: CLLocationCoordinate2D?
    public var locals = [Any]() {
        didSet {
            if locals.count != oldValue.count {
                hanleEmpty()
            }
            refreshFinished()
        }
    }

    // MARK: - Private Property -
    private let emptyView = MEmptyView()
    private let noNetworkView = MNoNetworkView()
    private var viewModel = MLocalViewModel()

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshList()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        viewModel.coordinate = coordinate
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        setupHeaderRefreshControl()
        setupBindings()
    }

    private func setupHeaderRefreshControl() {
        if coordinate != nil {
            refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        }
    }

    private func setupFooterRefreshControl() {
        tableView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
            self?.loadMore()
        }
    }

    private func setupRefreshControl() {
        if nil == tableView.cr.footer {
            setupFooterRefreshControl()
        }
    }

    private func setupBindings() {
        viewModel.list.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (result) in
                switch result {
                case .success(let list):
                    self?.locals = list
                case .failure(let error):
                    switch error {
                    case .underlying(_, _):
                        self?.hanleNetwork()
                    default:
                        self?.hanleEmpty()
                    }
                    self?.refreshFinished()
                    self?.showError(error: error)
                }
            }, onError: { [weak self] (error) in
                    self?.hanleNetwork()
                    self?.refreshFinished()
                    self?.showError(error: error)
            }).disposed(by: viewModel.disposeBag)
    }

    private func refreshList() {
        if locals.isEmpty && (coordinate != nil) {
            refreshControl?.beginAnimateRefreshing()
        }
    }

    @objc private func refresh() {
        viewModel.reloadAction.asObserver().onNext(())
    }

    private func loadMore() {
        viewModel.loadMoreAction.asObserver().onNext(())
    }

    private func refreshFinished() {
        self.tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.refreshControl?.endRefreshing()
            self.tableView.cr.endLoadingMore()
        })
    }

    private func hanleEmpty() {
        if locals.isEmpty {
            tableView.cr.removeFooter()
            emptyView.show(on: self) { [weak self] in
                self?.refresh()
            }
        } else {
            emptyView.hidden()
            noNetworkView.hidden()
            setupRefreshControl()
        }
    }

    private func hanleNetwork() {
        if locals.isEmpty {
            tableView.cr.removeFooter()
            noNetworkView.show(on: self) { [weak self] in
                self?.refresh()
            }
        }
    }

}


// MARK: - Table View Data Source Methods -
extension MLocalListViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locals.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""
        let object = locals[indexPath.row]
        if let local = object as? MLocal {
            switch local.style! {
            case .thumbnails:
                identifier = MLocalThumbnailsCell.identifier()
            case .thumbnail:
                identifier = MLocalThumbnailCell.identifier()
            case .onlyTitle:
                identifier = MLocalTitleCell.identifier()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MLocalCell
            cell.display(with: local)
            return cell
        } else if let adView = object as? BUNativeExpressAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: MAdvertCell.identifier(), for: indexPath) as! MAdvertCell
            cell.showAD(adView: adView)
            return cell
        }
        return UITableViewCell()
    }

}


// MARK: - Table View Delegate Methods -
extension MLocalListViewController {

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = locals[indexPath.row]
        if let adView = object as? BUNativeExpressAdView {
            return adView.frame.size.height
        }
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = locals[indexPath.row]
        if let local = object as? MLocal {
            if let url = local.url {
                showSafariViewController(with: url)
            }
        }
    }

}


import SafariServices


extension MLocalListViewController {

    fileprivate func showSafariViewController(with source: String) {
        guard let url = URL(string: source) else {
            return
        }
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.modalPresentationCapturesStatusBarAppearance = true
        present(safariViewController, animated: true, completion: nil)
    }

    fileprivate func jump2Safari(with source: String) {
        guard let url = URL(string: source) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}

