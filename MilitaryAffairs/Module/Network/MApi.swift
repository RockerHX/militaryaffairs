//
//  MApi.swift
//  News
//
//  Created by RockerHX on 2018/3/8.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import CoreLocation


enum MApi {

    enum Action: String {
        case refresh = "REFRESH"
        case page = "PAGE"
    }

    case app(id: String)
    case newses(cid: String, date: Int64?, action: Action)
    case newsDetail(id: String)
    case newsRecommends(id: String)
    case videos(category: String, date: Int64?, action: Action)
    case videoRecommends(id: String)
    case pushHistory
    case weather(coordinate: CLLocationCoordinate2D)
    case weathers(coordinate: CLLocationCoordinate2D)
    case log(count: Int, url: String, type: String, channel: String)
    case mutual(appID: String)
}


extension MApi: TargetType {

    /// The target's base `URL`.
    var baseURL: URL {
        #if DEBUG
            let base = "https://api.vslimit.com"
        #else
            let base = "https://api.vslimit.com"
        #endif
        return URL(string: base)!
    }

    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .app(let id):
            return "/apps/\(id)"
        case .newses:
            return "/news"
        case .newsDetail(let id):
            return "/news/\(id)"
        case .newsRecommends(let id):
            return "/news/\(id)/recommend"
        case .videos:
            return "/videos"
        case .videoRecommends(let id):
            return "/videos/\(id)/recommend"
        case .pushHistory:
            return "/pushs/\(MAppConfigurationManager.manager.configuration?.id ?? 0)"
        case .weather:
            return "/weather"
        case .weathers:
            return "/weather/days"
        case .log:
            return "/logs"
        case .mutual(let appID):
            return "/apps/\(appID)/mutual"
        }
    }

    /// The HTTP method used in the request.
    var method: Moya.Method {
        switch self {
        case .log:
            return .post
        default:
            return .get
        }
    }

    /// Provides stub data for use in testing.
    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }

    /// The type of HTTP task to be performed.
    var task: Task {
        switch self {
        case .app:
            return .requestPlain
        case .newses(let cid, let newsDate, let action):
            var parameters: [String: Any] = ["cid": cid,
                                             "aid": MAppConfigurationManager.AppID,
                                             "req": action.rawValue]
            if let date = newsDate {
                parameters["date"] = date
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case .newsDetail:
            return .requestPlain
        case .newsRecommends:
            return .requestPlain
        case .videos(let category, let newsDate, let action):
            var parameters: [String: Any] = ["category": category,
                                                  "aid": MAppConfigurationManager.AppID,
                                                  "req": action.rawValue]
            if let date = newsDate {
                parameters["date"] = date
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case .videoRecommends:
            return .requestPlain
        case .pushHistory:
            return .requestPlain
        case .weather(let coordinate):
            let parameters: [String: Any] = ["loc": "\(coordinate.latitude):\(coordinate.longitude)"]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case .weathers(let coordinate):
            let parameters: [String: Any] = ["loc": "\(coordinate.latitude):\(coordinate.longitude)"]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case .log(let count, let url, let type, let channel):
            let parameters: [String: Any] = ["count": count,
                                               "url": url,
                                              "type": type,
                                           "channel": channel]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case .mutual:
            return .requestPlain
        }
    }

    /// The headers to be used in the request.
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

