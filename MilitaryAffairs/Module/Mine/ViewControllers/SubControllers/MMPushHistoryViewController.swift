//
//  MMPushHistoryViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/23.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class MMPushHistoryViewController: UIViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    // MARK: - Private Property -
    private let service = MMineService()
    private let emptyView = MEmptyView()
    private var newsListController: MPNewsListViewController?
    private var newses = [MNews]() {
        didSet {
            newsListController?.newses = newses
            hiddenHud()
        }
    }

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if String(describing: MPNewsListViewController.self) == identifier {
                newsListController = segue.destination as? MPNewsListViewController
            }
        }
    }

    // MARK: - Event Methods -
    @IBAction func backButtonTaped() {
        navigationController?.popToRootViewController(animated: true)
    }

    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        fetchPushNewses()
    }
    
}


// MARK: - Private Methods -
extension MMPushHistoryViewController {

    private func fetchPushNewses() {
        showHud()
        service.fetchPushNewses()
        .done { [unowned self] newsList in
            if newsList.isEmpty {
                self.emptyView.show(on: self, retry: nil)
            } else {
                self.newses = newsList
            }
        }.catch { [unowned self] (error) in
            self.hiddenHud()
            self.showError(error: error)
            self.emptyView.show(on: self, retry: nil)
        }
    }

}

