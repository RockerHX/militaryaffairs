//
//  MAdvertPool.swift
//  News
//
//  Created by RockerHX on 2018/3/20.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Foundation


class MAdvertPool: NSObject {

    typealias ReloadClosure = (MAdvertPool) -> Void

    // MARK: Singleton
    static let shared = MAdvertPool()

    // MARK: Public Property
    public var adverts = [BUNativeExpressAdView]()
    public var advertsWeight = [BUNativeExpressAdView: Int]()

    // MARK: Private Property
    private let interval = 3
    private let loadCount = 3
    private let threshold = 20
    private var adManager: BUNativeExpressAdManager?
    private var reloadClosure = [ReloadClosure]()

    // MARK: Initialize Methods
    fileprivate override init() {
        super.init()
        let screenWidth = ((UIApplication.shared.delegate?.window)!)!.bounds.size.width
        let size = CGSize(width: screenWidth, height: screenWidth/1.78 + 60)

        let configuration = MAppConfigurationManager.manager.configuration
        if let platform = configuration?.platforms?.first {
            platform.adverts?.forEach({ (advert) in
                if let kind = advert.kind, kind == .native {
                    advert.subs?.forEach({ (nativeAdvert) in
                        if let style = nativeAdvert.style,
                            let posid = nativeAdvert.posid {
                            if style == .upTextDownImage {
                                setupADManager(with: posid, size: size)
                            }
                        }
                    })
                }
            })
        }
    }

}


// MARK: Private Methods
extension MAdvertPool {

    private func setupADManager(with slotID: String, size: CGSize) {
        let slot = BUAdSlot()
        slot.id = slotID
        slot.adType = .feed
        slot.imgSize = BUSize(by: .feed228_150)
        slot.position = .feed
        adManager = BUNativeExpressAdManager(slot: slot, adSize: size)
        adManager?.delegate = self
        adManager?.loadAdData(withCount: loadCount)
    }

    private func takeoutAdverts(count: Int) -> [BUNativeExpressAdView] {
        var weights = [BUNativeExpressAdView: Int]()
        adverts.forEach { [unowned self] (advert) in
            weights[advert] = self.advertsWeight[advert]
        }
        let sortedAdverts = weights.sorted { $0.value < $1.value }
        let lowExposureRateAdverts = sortedAdverts.map { $0.key }
        let intervalAdverts = lowExposureRateAdverts.prefix(count)
        return Array(intervalAdverts)
    }

}


// MARK: Public Methods
extension MAdvertPool {

    public func reload(success: @escaping ReloadClosure) {
        reloadClosure.append(success)
        if adverts.count >= threshold {
            reloadClosure.removeFirst()(self)
        } else {
            if nil == adManager {
                reloadClosure.removeFirst()(self)
            } else {
                adManager?.loadAdData(withCount: loadCount)
            }
        }
    }

    public func packageList(list: [Any]) -> [Any] {
        adManager?.loadAdData(withCount: loadCount)
        if nil == adManager {
            return list
        }
        var source = Array(list)
        var collection = [Any]()
        let adCount = source.count / interval
        let ads = takeoutAdverts(count: adCount)
        repeat {
            ads.forEach { (advert) in
                if source.count > interval {
                    (0..<interval).forEach({ _ in
                        collection.append(source.removeFirst())
                    })
                    collection.append(advert)
                }
            }
        } while (source.count > interval) && (ads.count > 0)

        collection.append(contentsOf: source)
        return collection
    }

}


// MARK: BUNativeExpressAdViewDelegate Methods
extension MAdvertPool: BUNativeExpressAdViewDelegate {

    func nativeExpressAdSuccess(toLoad nativeExpressAd: BUNativeExpressAdManager, views: [BUNativeExpressAdView]) {
        let last = views.count
        let first = (last < loadCount) ? 0 : (last - loadCount)
        let newAds = Array(views[first..<last])
        adverts.append(contentsOf: newAds)
        let entry = ((UIApplication.shared.delegate?.window)!)!.rootViewController as! MEntryViewController
        let mainViewController = entry.mainViewController!
        newAds.forEach { [unowned self] (advert) in
            self.advertsWeight[advert] = 0
            advert.removeFromSuperview()
            advert.rootViewController = mainViewController
            advert.render()
        }
        if !reloadClosure.isEmpty {
            reloadClosure.removeFirst()(self)
        }
    }

    func nativeExpressAdFail(toLoad nativeExpressAd: BUNativeExpressAdManager, error: Error?) {
        print("nativeExpressAdFail: \(String(describing: error))")
        if !reloadClosure.isEmpty {
            reloadClosure.removeFirst()(self)
        }
    }

    func nativeExpressAdViewWillShow(_ nativeExpressAdView: BUNativeExpressAdView) {
        if let weight = advertsWeight[nativeExpressAdView] {
            advertsWeight[nativeExpressAdView] = (weight + 1)
        }
    }

}

