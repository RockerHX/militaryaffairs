//
//  MDChannel.swift
//  News
//
//  Created by RockerHX on 2018/3/2.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift


class MDChannel: Object {

    typealias CodeClosure = () -> Void

    @objc dynamic var id: String? = nil
    @objc dynamic var name: String? = nil
    @objc dynamic var type: String? = nil
    @objc dynamic var status: String? = nil
    @objc dynamic var select = true

    @objc dynamic var createdDate = Date()
    @objc dynamic var updatedDate: Date? = nil

    override static func indexedProperties() -> [String] {
        return ["id", "createdDate", "updatedDate", "expiredDate"]
    }

    override static func primaryKey() -> String? {
        return "id"
    }

}


// MARK: - Static Methods -
extension MDChannel {

    static func fetchAll() -> [MDChannel]? {
        return filter(nil)
    }

    static func fetchAvailable() -> [MDChannel]? {
        return filter("select == YES AND status == 'NORMAL'")
    }

    static func fetchDressChannels() -> [MDChannel]? {
        return filter("type == 'MILITARYAFFAIRS' AND status == 'NORMAL'")
    }

    static func fetchVideoChannels() -> [MDChannel]? {
        return filter("type == 'VIDEO' AND status == 'NORMAL'")
    }

    static func fetchAvailableDressChannels() -> [MDChannel]? {
        return filter("type == 'MILITARYAFFAIRS' AND status == 'NORMAL' AND select == YES")
    }

    static func fetchAvailableVideoChannels() -> [MDChannel]? {
        return filter("type == 'VIDEO' AND status == 'NORMAL' AND select == YES")
    }

    static func filter(_ predicate: String?) -> [MDChannel]? {
        var channels = [MDChannel]()
        do {
            let realm = try Realm()
            var results: Results<MDChannel>
            if let filterCondition = predicate {
                results = realm.objects(MDChannel.self).filter(filterCondition)
            } else {
                results = realm.objects(MDChannel.self)
            }
            results.forEach({ (channel) in
                if (channel.id != nil) && (channel.name != nil) {
                    channels.append(channel)
                }
            })
            return channels
        } catch {
            debugPrint(error)
            return nil
        }
    }

}


// MARK: - Public Methods -
extension MDChannel {

    public func save(update: Bool = false, code: CodeClosure) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                updatedDate = Date()
                code()
                if !update {
                    realm.add(self, update: .all)
                }
            }
        } catch {
            debugPrint(error)
            return false
        }
        return true
    }

    public func changeSelectState() -> Bool {
        return save(update: true, code: {
            select = !select
        })
    }

    public func update(code: CodeClosure) -> Bool {
        return save(update: true, code: code)
    }

}

