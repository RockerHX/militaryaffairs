//
//  MNewsCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Devices


class MNewsCell: UITableViewCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var provenanceLabel: UILabel?
    @IBOutlet var timeLabel: UILabel?

    @IBOutlet var fakeCountLabel: UILabel?
    @IBOutlet var favoriteCountLabel: UILabel?

    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Public Methods -
    public func display(withNews news: MNews) {
        titleLabel?.text = news.title
        provenanceLabel?.text = MAppConfigurationManager.manager.auditing ? Device.Application.shared.name : news.source
        timeLabel?.isHidden = true
//        timeLabel?.text = news.localeTime

        fakeCountLabel?.text = news.praise
        favoriteCountLabel?.text = news.collect
    }
    
    // MARK: - Private Methods -

}

