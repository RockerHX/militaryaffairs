//
//  PagesContainerViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/5.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


protocol PagesContainerViewControllerDelegate {
    func containerPageChanged(toIndex index: Int)
}

class PagesContainerViewController: UIPageViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var containerDelegate: PagesContainerViewControllerDelegate?
    public var displayControllers = [UIViewController]() {
        didSet {
            showViewController(controller: displayControllers.first)
        }
    }

    public var index = 0 {
        didSet {
            direction = (index > oldValue) ? .forward : .reverse
            showViewController(controller: displayControllers[safe: index], animated: true)
        }
    }

    // MARK: - Private Property -
    private var direction: UIPageViewController.NavigationDirection = .forward

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        dataSource = self
        delegate = self
    }

    private func showViewController(controller: UIViewController?, animated: Bool = false) {
        if let viewController = controller {
            setViewControllers([viewController], direction: direction, animated: animated, completion: nil)
        }
    }

}


// MARK: - UIPageViewController Data Source Methods -
extension PagesContainerViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if var index = displayControllers.firstIndex(of: viewController) {
            if (index == 0) || (index == NSNotFound) {
                return nil
            }

            index -= 1
            return displayControllers[index]
        } else {
            return nil
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if var index = displayControllers.firstIndex(of: viewController) {
            if NSNotFound == index {
                return nil
            }

            index += 1
            if displayControllers.count == index {
                return nil
            }
            return displayControllers[index]
        } else {
            return nil
        }
    }

}


// MARK: - UIPageViewController Delegate Methods -
extension PagesContainerViewController: UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let controller = pendingViewControllers.first, let index = displayControllers.firstIndex(of: controller) {
            containerDelegate?.containerPageChanged(toIndex: index)
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let controller = previousViewControllers.first, let index = displayControllers.firstIndex(of: controller) {
            if !completed {
                containerDelegate?.containerPageChanged(toIndex: index)
            }
        }
    }

}
