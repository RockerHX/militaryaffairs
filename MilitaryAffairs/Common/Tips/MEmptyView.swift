//
//  MEmptyView.swift
//  BulletinTopLine
//
//  Created by RockerHX on 2018/4/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


class MEmptyView: HXXibView {

    typealias Closure = () -> ()

    // MARK: - IBOutlet Property -
    @IBOutlet var container: UIView?
    @IBOutlet var textLabel: UILabel?
    @IBOutlet var indicatorView: UIActivityIndicatorView?

    // MARK: - Public Property -
    public private(set) var canRetry = true

    // MARK: - Private Property -
    private var closure: Closure?

    // MARK: - Event Methods -
    @objc private func actionTaped() {
        showIndicatorView()
        if let retryClosure = closure {
            retryClosure()
        }
    }

    // MARK: - Private Methods -
    private func configure() {
        if canRetry {
            indicatorView?.isHidden = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionTaped))
            addGestureRecognizer(tapGesture)
        } else {
            hiddenIndicatorView()
        }
    }

    private func showIndicatorView() {
        indicatorView?.isHidden = false
        indicatorView?.startAnimating()
    }

    private func hiddenIndicatorView() {
        indicatorView?.stopAnimating()
        indicatorView?.isHidden = true
    }

    // MARK: - Public Methods -
    public func show(on viewController: UIViewController, retry: Closure?) {
        if nil == retry {
            canRetry = false
            textLabel?.text = "暂无数据"
        }
        configure()
        closure = retry
        frame = viewController.view.frame
        viewController.view.addSubview(self)
    }

    public func hidden(from viewController: UIViewController? = nil) {
        hiddenIndicatorView()
        if let superView = viewController?.view {
            superView.willRemoveSubview(self)
        } else {
            removeFromSuperview()
        }
    }

    public func stopLoading() {
        hiddenIndicatorView()
    }

}

