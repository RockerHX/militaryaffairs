//
//  MNoNetworkView.swift
//  BulletinTopLine
//
//  Created by RockerHX on 2018/4/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


class MNoNetworkView: HXXibView {

    typealias Closure = () -> ()

    // MARK: - IBOutlet Property -
    @IBOutlet var container: UIView?
    @IBOutlet var indicatorView: UIActivityIndicatorView?
    
    // MARK: - Public Property -
    // MARK: - Private Property -
    private var closure: Closure?

    // MARK: - Event Methods -
    @objc private func actionTaped() {
        if let retryClosure = closure {
            retryClosure()
        }
        showIndicatorView()
    }

    // MARK: - Public Methods -
    public func show(on viewController: UIViewController, retry: @escaping Closure) {
        configure()
        closure = retry
        frame = viewController.view.frame
        viewController.view.addSubview(self)
    }

    public func hidden() {
        hiddenIndicatorView()
        removeFromSuperview()
    }

    // MARK: - Private Methods -
    private func configure() {
        indicatorView?.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionTaped))
        addGestureRecognizer(tapGesture)
    }

    private func showIndicatorView() {
        indicatorView?.isHidden = false
        indicatorView?.startAnimating()
    }

    private func hiddenIndicatorView() {
        indicatorView?.stopAnimating()
        indicatorView?.isHidden = true
    }

}

