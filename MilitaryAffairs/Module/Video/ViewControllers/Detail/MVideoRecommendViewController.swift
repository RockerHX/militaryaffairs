//
//  MVideoRecommendViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/17.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class MVideoRecommendViewController: UITableViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var id: Int?

    // MARK: - Private Property -
    private let service = MVideoService()
    private var recommends = [Any]() {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                let section = IndexSet(integer: 0)
                self.tableView.reloadSections(section, with: .bottom)
            })
        }
    }

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        fetchRecommens()
    }

    private func fetchRecommens() {
        if let vodeoID = id.string {
            service.fetchVideoRecommends(withVideoID: vodeoID)
                .done { [weak self] fetchedRecommends in
                    MDetailAdvertPool.shared.reload(success: { (pool) in
                        self?.recommends = pool.packageList(list: fetchedRecommends)
                    })
                }.catch { error in
                    debugPrint(error)
                }
        }
    }

}


// MARK: - Table View Data Source Methods -
extension MVideoRecommendViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recommends.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = recommends[indexPath.row]
        if let video = object as? MVideo {
            let cell = tableView.dequeueReusableCell(withIdentifier: MVideoRecommendCell.identifier(), for: indexPath) as! MVideoRecommendCell
            cell.dispay(withVideo: video)
            return cell
        } else if let adView = object as? BUNativeExpressAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: MAdvertCell.identifier(), for: indexPath) as! MAdvertCell
            cell.showAD(adView: adView)
            return cell
        }
        return UITableViewCell()
    }

}


// MARK: - Table View Delegate Methods -
extension MVideoRecommendViewController {

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = recommends[indexPath.row]
        if let adView = object as? BUNativeExpressAdView {
            return adView.frame.size.height
        }
        return tableView.rowHeight
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = recommends[indexPath.row]
        if let video = object as? MVideo {
            video.readed()
            let detailViewController = MVideoDetailViewController.instance()
            detailViewController.video = video
            navigationController?.pushViewController(detailViewController, animated: true)
        }
    }

}

