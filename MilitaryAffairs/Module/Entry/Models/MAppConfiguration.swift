//
//  MAppConfiguration.swift
//  News
//
//  Created by RockerHX on 2018/3/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift


class MAppConfiguration: Codable {

    enum State: String, Codable {
        case normal = "NORMAL"
        case hidden = "HIDDEN"
        case stop   = "STOP"
        case delete = "DELETE"
    }

    enum LocationState: Int, Codable {
        case hidden    = -1
        case local     = 0
        case operation = 1
    }

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case logo
        case color
        case version
        case locationState  = "loc"
        case locationName   = "loc_name"
        case newsChannels   = "channels"
        case videoChannels
        case state          = "status"
        case appKey         = "app_key"
        case appSecret      = "app_secret"
        case platforms      = "spaces"
    }

    var id: Int? = nil
    var name: String? = nil
    var logo: String? = nil
    var color: String? = nil
    var version: String? = nil
    var locationState: LocationState? = nil
    var locationName: String? = nil
    var newsChannels: [MChannel]? = nil
    var videoChannels: [MChannel]? = nil
    var state: State? = nil
    var appKey: String? = nil
    var appSecret: String? = nil
    var platforms: [Platform]? = nil

}


extension MAppConfiguration {

    struct Platform: Codable {

        enum CodingKeys: String, CodingKey {
            case id
            case name
            case code
            case appid
            case state   = "status"
            case adverts = "adposes"
        }

        var id: Int?
        var name: String?
        var code: String?
        var appid: String?
        var state: State?
        var adverts: [Advert]?
    }

}


extension MAppConfiguration.Platform {

    struct Advert: Codable {

        enum Kind: String, Codable {
            case splash = "SCREEN"
            case banner = "BANNER"
            case native = "NATIVE"
            case insert = "INSERT"
        }

        enum CodingKeys: String, CodingKey {
            case id
            case name
            case posid
            case kind = "type"
            case subs = "adposList"
        }

        var id: Int?
        var name: String?
        var posid: String?
        var kind: Kind?
        var subs: [NativeAdvert]?
    }

}


extension MAppConfiguration.Platform.Advert {

    struct NativeAdvert: Codable {

        enum Style: String, Codable {
            case upImageDownText    = "UPIMG"
            case upTextDownImage    = "DOWNIMG"
            case leftImageRigthText = "LEFTIMG"
            case leftTextRightImage = "RIGHTIMG"
            case doubleImages       = "DOUBLEIMG"
            case image1280_720      = "IMG1280"
            case image720_1280      = "IMG800"
        }

        enum CodingKeys: String, CodingKey {
            case id
            case name
            case posid
            case style = "mode"
        }

        var id: Int?
        var name: String?
        var posid: String?
        var style: Style?
    }

}


extension MAppConfiguration {

    enum Path: String {
        case relative = "/Configuration"
        case fileName = "configuration.data"
    }

    private func cacheChannel() {
        if let channels = newsChannels {
            cacheChannels(channels)
        }
        if let channels = videoChannels {
            cacheChannels(channels, isVideo: true)
        }
    }

    @discardableResult
    private func archive() -> Bool {
        let storePath = HXPathManager.manager.storePath(withDirectory: .Document, relativePath: Path.relative.rawValue, fileName: Path.fileName.rawValue)
        if let path = storePath {
            let url = URL(fileURLWithPath: path)
            do {
                let data = try JSONEncoder().encode(self)
                try data.write(to: url)
                return true
            } catch {
                debugPrint(error)
                return false
            }
        }
        return false
    }

    static func unArchive() -> Any? {
        let storePath = HXPathManager.manager.storePath(withDirectory: .Document, relativePath: Path.relative.rawValue, fileName: Path.fileName.rawValue)
        if let path = storePath {
            let url = URL(fileURLWithPath: path)
            do {
                let data = try Data(contentsOf: url, options: .alwaysMapped)
                let objct = try JSONDecoder().decode(MAppConfiguration.self, from: data)
                return objct
            } catch {
                debugPrint(error)
                return nil
            }
        }
        return nil
    }

}


extension MAppConfiguration {

    private func cacheChannels(_ channels: [MChannel], isVideo: Bool = false) {
        channels.forEach { (channel) in
            channel.cache(isVideo: isVideo)
        }
    }

    public func cache() {
        cacheChannel()
        archive()
    }

}



