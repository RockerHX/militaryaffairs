//
//  PagesViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/5.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class PagesViewController: UIViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var titles: [String]?
    public var viewControllers: [UIViewController]?

    // MARK: - Private Property -
    private var controlViewController: PagesControlViewController?
    private var containerViewController: PagesContainerViewController?

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if String(describing: PagesControlViewController.self) == identifier {
                controlViewController = segue.destination as? PagesControlViewController
                controlViewController?.delegate = self
            } else if String(describing: PagesContainerViewController.self) == identifier {
                containerViewController = segue.destination as? PagesContainerViewController
                containerViewController?.containerDelegate = self
            }
        }
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    public func reloadData() {
        if let displayTitles = titles, let displayControllers = viewControllers {
            if let controlTitles = controlViewController?.titles {
                if displayTitles != controlTitles {
                    containerViewController?.displayControllers = displayControllers
                    controlViewController?.titles = displayTitles
                }
            }
        }
    }

    // MARK: - Private Methods -
    private func configure() {
    }
    
}


// MARK: - PagesControlViewControllerDelegate -
extension PagesViewController: PagesControlViewControllerDelegate {

    func controlPageChanged(toIndex index: Int) {
        containerViewController?.index = index
    }

}


// MARK: - PagesContainerViewControllerDelegate -
extension PagesViewController: PagesContainerViewControllerDelegate {

    func containerPageChanged(toIndex index: Int) {
        controlViewController?.index = index
    }

}

