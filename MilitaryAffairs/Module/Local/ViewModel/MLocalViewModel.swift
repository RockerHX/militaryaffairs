//
//  MLocalViewModel.swift
//  MilitaryAffairs
//
//  Created by RockerHX on 2018/6/22.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RxSwift
import Moya
import CoreLocation


class MLocalViewModel: NSObject {

    // MARK: - Public Property -
    public var coordinate: CLLocationCoordinate2D?
    public let disposeBag = DisposeBag()

    // MARK: - Private Property -
    private var offset = 0
    private let count = 20
    private let service = MLocalService()
    private var listCollection = [Any]()

    // MARK: - Inputs -
    let reloadAction = PublishSubject<Void>()
    let loadMoreAction = PublishSubject<Void>()

    // MARK: - Outputs -
    public private(set) var list = PublishSubject<Result<[Any], MoyaError>>()

    // MARK: - Initialize Methods -
    override init() {
        super.init()

        setupBinding()
    }

    private func setupBinding() {
        reloadAction
            .subscribe { [weak self] _ in
                self?.reload()
            }.disposed(by: disposeBag)

        loadMoreAction
            .subscribe { [weak self] _ in
                self?.loadMore()
            }.disposed(by: disposeBag)
    }

}


extension MLocalViewModel {

    private func reload() {
        listCollection.removeAll()
        fetchLocals()
    }

    private func loadMore() {
        fetchMoreLocals()
    }

    private func fetchLocals() {
        if let coord = coordinate {
            service.fetchLocals(offset: offset, count: count, with: coord)
                .done { [weak self] localList in
                    self?.offset += self?.count
                    self?.updateCollection(localList)
                }.catch { [weak self] (error) in
                    if let e = error as? MoyaError {
                        self?.list.onNext(.failure(e))
                    }
                }
        }
    }

    private func fetchMoreLocals() {
        if let coord = coordinate {
            service.fetchLocals(offset: offset, count: count, with: coord)
                .done { [weak self] localList in
                    self?.offset += self?.count
                    self?.appendCollection(localList)
                }.catch { [weak self] (error) in
                    if let e = error as? MoyaError {
                        self?.list.onNext(.failure(e))
                    }
                }
        }
    }

    private func updateCollection(_ collection: [Any]) {
        MAdvertPool.shared.reload { (pool) in
            let list = pool.packageList(list: collection)
            self.listCollection = collection
            self.list.onNext(.success(list))
        }
    }

    private func appendCollection(_ collection: [Any]) {
        let packageList = MAdvertPool.shared.packageList(list: collection)
        listCollection.append(contentsOf: packageList)
        list.onNext(.success(listCollection))
    }

}

