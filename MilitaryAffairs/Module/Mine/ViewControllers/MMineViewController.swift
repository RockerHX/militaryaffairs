//
//  MMineViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import Kingfisher
import StoreKit


class MMineViewController: UITableViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var cacheLabel: UILabel?
    @IBOutlet var nightModeSwitch: UISwitch?
    @IBOutlet var brightnessSlider: UISlider?

    // MARK: - Public Property -
    // MARK: - Private Property -
    private var cache: String = "0M" {
        didSet {
            cacheLabel?.text = cache
        }
    }

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        calculateDiskCacheSize()
        loadBrightness()
    }

    /*
    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Event Methods -
    @IBAction func nightModeSwitchValueChanged(sender: UISwitch) {
        UIScreen.main.brightness = CGFloat(sender.isOn ? 0.2 : (brightnessSlider?.value ?? 0))
    }

    @IBAction func brightnessSliderValueChanged(sender: UISlider) {
        nightModeSwitch?.setOn(false, animated: true)
        UIScreen.main.brightness = CGFloat(sender.value)
    }

    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        calculateDiskCacheSize()
    }
    
}


// MARK: - Inline Enum -
extension MMineViewController {

    enum FunctionRow: Int {
        case myFavorites = 0
        case browsingHistory
        case propellingMovement
        case recommendFriends
        case favourableComment
    }

    enum ControlRow: Int {
        case cleanCache = 0
        case nightMode
        case screenIntensity
    }

    enum OtherRow: Int {
        case about = 0
        case secret = 1
    }

    enum Section: Int {
        case function = 0
        case control
        case other
    }

}

// MARK: - Private Methods -
extension MMineViewController {

    private func calculateDiskCacheSize() {
        KingfisherManager.shared.cache.calculateDiskStorageSize { (result) in
            switch result {
            case .success(let size):
                let cacheSize: Double = Double(size)/(1024.0*1024.0)
                self.cache = "\(String(format: "%.2f", cacheSize))M"
            case .failure(let error):
                debugPrint(error.errorDescription ?? "")
            }
        }
    }

    private func cleanCache() {
        let cachePrompt = "当前存在 \(cache) 缓存"
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let cleanAction = UIAlertAction(title: "清除", style: .default, handler: { _ in
            KingfisherManager.shared.cache.cleanExpiredDiskCache()
            KingfisherManager.shared.cache.clearDiskCache()
            self.calculateDiskCacheSize()
        })
        let alert = UIAlertController(title: "是否清理", message: cachePrompt, preferredStyle: .actionSheet)
        alert.addAction(cancelAction)
        alert.addAction(cleanAction)
        present(alert, animated: true, completion: nil)
    }

    private func loadBrightness() {
        brightnessSlider?.value = Float(UIScreen.main.brightness)
    }

    private func favourableComment() {
//        if #available(iOS 10.3, *) {
//            UIApplication.shared.keyWindow?.endEditing(true)
//            SKStoreReviewController.requestReview()
//        } else {
            let appID = "1271673198"
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appID)?action=write-review") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
//        }
    }

}


// MARK: - Table View Delegate Methods -
extension MMineViewController {

    // MARK: - Table View Delegate Methods -
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let section = Section(rawValue: indexPath.section) {
            switch section {
            case .function:
                if let row = FunctionRow(rawValue: indexPath.row) {
                    switch row {
                    case .favourableComment:
                        favourableComment()
                    case .recommendFriends:
                        MShareService.shareApp()
                    default:
                        break
                    }
                }
            case .control:
                if let row = ControlRow(rawValue: indexPath.row) {
                    switch row {
                    case .cleanCache:
                        cleanCache()
                    default:
                        break
                    }
                }
            case .other:
                guard let row = OtherRow(rawValue: indexPath.row) else { return }
                switch row {
                case .secret:
                    showSafariViewController(with: "http://js.vslimit.net/privacyx.html")
                default:
                    break
                }
            }
        }
    }

}

