//
//  MPNewsListViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import PromiseKit
import CRRefresh
import RxSwift


class MPNewsListViewController: UITableViewController {

    typealias ListType = ModuleFlag
    // MARK: - IBOutlet Property
    // MARK: - Public Property
    public var id: String?
    public var canDelete = false
    public var type: ListType = .favorite
    public var newses = [Any]() {
        didSet {
            if shouldReload {
                if newses.count != oldValue.count {
                    hanleEmpty()
                }
                refreshFinished()
            } else {
                hanleEmpty()
            }
        }
    }

    // MARK: - Private Property
    private let emptyView = MEmptyView()
    private let noNetworkView = MNoNetworkView()
    private var viewModel = MNewsViewModel()
    private var shouldReload = true

    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshList()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if NPrincipalSegue.List2Detial.identifier == identifier {
                let detailViewController = segue.destination as? MNewsDetailViewController
                detailViewController?.news = sender as? MNews
            } else if let typedInfo = R.segue.mpNewsListViewController.list2Web(segue: segue) {
                let news = sender as? MNews
                let webViewController = typedInfo.destination
                webViewController.title = news?.title
                webViewController.url = news?.url
            } else if let typedInfo = R.segue.mpNewsListViewController.go2VideoDetail(segue: segue) {
                let videoDetailViewController = typedInfo.destination
                videoDetailViewController.news = sender as? MNews
            }
        }
    }

    // MARK: - Event Methods
    // MARK: - Public Methods
    // MARK: - Private Methods
    private func configure() {
        viewModel.cid = id
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        setupHeaderRefreshControl()
        setupBindings()
    }

    private func setupHeaderRefreshControl() {
        if id != nil {
            refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        }
    }

    private func setupFooterRefreshControl() {
        tableView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
            self?.loadMore()
        }
    }

    private func setupRefreshControl() {
        if nil == tableView.cr.footer {
            setupFooterRefreshControl()
        }
    }

    private func setupBindings() {
        viewModel.list.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (result) in
                switch result {
                case .success(let list):
                    self?.newses = list
                case .failure(let error):
                    switch error {
                    case .underlying(_, _):
                        self?.hanleNetwork()
                    default:
                        self?.hanleEmpty()
                    }
                    self?.refreshFinished()
                    self?.showError(error: error)
                }
            }, onError: { [weak self] (error) in
                    self?.hanleNetwork()
                    self?.refreshFinished()
                    self?.showError(error: error)
            }).disposed(by: viewModel.disposeBag)
    }

    private func refreshList() {
        if newses.isEmpty && (id != nil) {
            refreshControl?.beginAnimateRefreshing()
        }
    }

    @objc private func refresh() {
        viewModel.reloadAction.asObserver().onNext(())
    }

    private func loadMore() {
        viewModel.loadMoreAction.asObserver().onNext(())
    }

    private func refreshFinished() {
        self.tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.refreshControl?.endRefreshing()
            self.tableView.cr.endLoadingMore()
        })
    }

    private func hanleEmpty() {
        if newses.isEmpty {
            tableView.cr.removeFooter()
            emptyView.show(on: self) { [weak self] in
                self?.refresh()
            }
        } else {
            emptyView.hidden()
            noNetworkView.hidden()
            if !canDelete {
                setupRefreshControl()
            }
        }
    }

    private func hanleNetwork() {
        if newses.isEmpty {
            tableView.cr.removeFooter()
            noNetworkView.show(on: self) { [weak self] in
                self?.refresh()
            }
        }
    }

}


extension MPNewsListViewController: BoardInstance {

    enum Board: String, BoardType {
        case Principal
    }

    static func instance() -> MPNewsListViewController {
        return EasyBoard<Board, MPNewsListViewController>.viewController(storyBoard: .Principal)
    }
}


// MARK: - Table View Data Source Methods
extension MPNewsListViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newses.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""
        let object = newses[indexPath.row]
        if let news = object as? MNews {
            switch news.style! {
            case .thumbnails:
                identifier = MNewsThumbnailsCell.identifier()
            case .cover:
                identifier = MNewsCoverCell.identifier()
            case .thumbnail:
                identifier = MNewsThumbnailCell.identifier()
            case .summary:
                identifier = MNewsSummaryCell.identifier()
            case .onlyTitle:
                identifier = MNewsTitleCell.identifier()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MNewsCell
            cell.display(withNews: news)
            return cell
        } else if let adView = object as? BUNativeExpressAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: MAdvertCell.identifier(), for: indexPath) as! MAdvertCell
            cell.showAD(adView: adView)
            return cell
        }
        return UITableViewCell()
    }

}


// MARK: - Table View Delegate Methods
extension MPNewsListViewController {

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = newses[indexPath.row]
        if let adView = object as? BUNativeExpressAdView {
            return adView.frame.size.height
        }
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = newses[indexPath.row]
        guard let news = object as? MNews,
              let mode = news.mode
        else { return }
        switch mode {
        case .native:
            if let type = news.type, let videoURL = news.videoURL {
                if (type == .video) && !videoURL.isEmpty {
                    performSegue(withIdentifier: R.segue.mpNewsListViewController.go2VideoDetail, sender: news)
                    return
                }
            }
            NPrincipalSegue.List2Detial.performSegue(on: self, sender: news)
        case .store:
            if let source = news.url, let url = URL(string: source) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case .web:
            performSegue(withIdentifier: R.segue.mpNewsListViewController.list2Web, sender: news)
        }
    }

}


extension MPNewsListViewController {

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return canDelete
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if .delete == editingStyle {
            if let news = newses[indexPath.row] as? MNews {
                switch type {
                case .favorite:
                    news.unFavorite()
                case .read:
                    news.unRead()
                }
            }
            shouldReload = false
            newses.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }

}


import SafariServices

extension MPNewsListViewController {

    fileprivate func showSafariViewController(with source: String) {
        guard let url = URL(string: source) else {
            return
        }
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.modalPresentationCapturesStatusBarAppearance = true
        present(safariViewController, animated: true, completion: nil)
    }

    fileprivate func jump2Safari(with source: String) {
        guard let url = URL(string: source) else {
            return
        }
        UIApplication.shared.canOpenURL(url)
    }

}

