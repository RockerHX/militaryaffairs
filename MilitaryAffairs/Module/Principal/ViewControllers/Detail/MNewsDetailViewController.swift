//
//  MNewsDetailViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import WebKit
import PromiseKit
import SKPhotoBrowser
import Devices


class MNewsDetailViewController: UITableViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var favoriteButton: UIButton?
    @IBOutlet var headerView: UIView?
    @IBOutlet var newsHeaderContainer: UIView?
    @IBOutlet var newsTitleLabel: UILabel?
    @IBOutlet var newsTimeLabel: UILabel?
    @IBOutlet var newsSourceLabel: UILabel?

    // MARK: - Public Property -
    public var news: MNews?

    // MARK: - Private Property -
    private var newsContentView: WKWebView?
    private let service = MNewsService()
    private var recommends = [Any]() {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                let section = IndexSet(integer: 0)
                self.tableView.reloadSections(section, with: .bottom)
            })
        }
    }
    private var imageURLs = [String]()

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if recommends.isEmpty {
            fetchDetail()
        }
    }

    // MARK: - Event Methods -
    @IBAction func backButtonTaped() {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func favoriteButtonTaped(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        news?.favorited(sender.isSelected)
    }

    @IBAction func shareButtonTaped() {
        if let title = news?.title,
            let url = news?.url {
            MShareService.share(withTitle: title, prompt: (news?.summary ?? title), url: url)
        }
    }

    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        showHud()
    }

    private func fetchDetail() {
        fetchDetailContent()
        displayFavoriteState()
    }

    private func displayFavoriteState() {
        if let favorite = news?.favorite() {
            favoriteButton?.isSelected = favorite
        }
    }

    private func displayNewsHeader() {
        newsTitleLabel?.text = news?.title
        newsTimeLabel?.text = news?.displayTime
        newsSourceLabel?.text = MAppConfigurationManager.manager.auditing ? Device.Application.shared.name : news?.source
    }

    private func fetchRecommens() {
        if let id = news?.id.string {
            service.fetchNewsRecommends(withNewsID: id)
                .done { [weak self] fetchedRecommends in
                    MDetailAdvertPool.shared.reload(success: { (pool) in
                        self?.recommends = pool.packageList(list: fetchedRecommends)
                    })
                }.catch { error in
                    debugPrint(error)
                }
        }
    }

    private func fetchDetailContent() {
        if let id = news?.id.string {
            service.fetchNewsDetailContent(withID: id)
                .done { [weak self] (content) in
                    self?.updateNews(with: content)
                }.catch { error in
                    debugPrint(error)
                }
        }
    }

    private func updateNews(with content: String) {
        news?.content = content
        news?.cache()
        displayNewsHeader()
        loadNewsContentView()
    }

}


extension MNewsDetailViewController: BoardInstance {

    enum Board: String, BoardType {
        case Principal
    }

    static func instance() -> MNewsDetailViewController {
        return EasyBoard<Board, MNewsDetailViewController>.viewController(storyBoard: .Principal)
    }
}


// MARK: - Table View Data Source Methods -
extension MNewsDetailViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recommends.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = recommends[indexPath.row]
        if let news = object as? MNews {
            let cell = tableView.dequeueReusableCell(withIdentifier: MNewsThumbnailCell.identifier(), for: indexPath) as! MNewsThumbnailCell
            cell.display(withNews: news)
            return cell
        } else if let adView = object as? BUNativeExpressAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: MAdvertCell.identifier(), for: indexPath) as! MAdvertCell
            cell.showAD(adView: adView)
            return cell
        }
        return UITableViewCell()
    }

}


// MARK: - Table View Delegate Methods -
extension MNewsDetailViewController {

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = recommends[indexPath.row]
        if let adView = object as? BUNativeExpressAdView {
            return adView.frame.size.height
        }
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: MNewsDetailRecommendPromptCell.identifier())
        cell?.contentView.backgroundColor = .white
        return (recommends.count > 0) ? cell?.contentView : nil
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (recommends.count > 0) ? 44 : 0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = recommends[indexPath.row]
        if let news = object as? MNews {
            news.readed()

            let detailViewController = MNewsDetailViewController.instance()
            detailViewController.news = news
            navigationController?.pushViewController(detailViewController, animated: true)
        }
    }

}


// MARK: - WebView Configuration Methods -
extension MNewsDetailViewController {

    private func loadNewsContentView() {
        let webView = setupNewsContentView()
        newsContentView = webView
        headerView?.addSubview(webView)
        if let HTMLString = news?.content {
            webView.loadHTMLString(HTMLString, baseURL: nil)
        }
    }

    private func setupNewsContentView() -> WKWebView {
        let webCofiguration = setupWebConfiguration()
        let width = view.bounds.size.width
        let react = CGRect(x: 0, y: 0, width: width, height: 1)
        let webView = WKWebView(frame: react, configuration: webCofiguration)
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false
        return webView
    }

    private func setMetaScript() -> WKUserScript {
        let js = """
                var meta = document.createElement('meta');
                meta.setAttribute('name', 'viewport');
                meta.setAttribute('content', 'width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no');
                document.getElementsByTagName('head')[0].appendChild(meta);
                """
        return WKUserScript(source: js, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    }

    private func getImagesScript() -> WKUserScript {
        let js = """
                function getImages() {
                    var objects = document.getElementsByTagName("img");
                    var urls = new Array(objects.length);
                    for(var i = 0; i < objects.length; i++) {
                        objects[i].onclick = function() {
                            document.location="news:imageClick:"+this.src;
                        };
                        urls[i] = objects[i].src;
                    };
                    return urls;
                };
                """
        return WKUserScript(source: js, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    }

    private func unabledTouchCalloutScript() -> String {
        return """
        function unabledTouchCallout() {
        var objects = document.getElementsByTagName("img");
        for(var i = 0; i < objects.length; i++) {
        objects[i].style.webkitTouchCallout='none';
        objects[i].style.webkitUserSelect='none';
        };
        };
        unabledTouchCallout();
        """
    }

    private func setupWebConfiguration() -> WKWebViewConfiguration {
        let contentController = WKUserContentController()
        contentController.addUserScript(setMetaScript())
        contentController.addUserScript(getImagesScript())
        let preferences = WKPreferences()
        preferences.minimumFontSize = 15
        let webCofiguration = WKWebViewConfiguration()
        webCofiguration.userContentController = contentController
        webCofiguration.preferences = preferences
        return webCofiguration
    }

}


// MARK: - WebView NavigationAction Methods -
extension MNewsDetailViewController {

    private func refreshHeader(withHeight refreshHeight: CGFloat) {
        let screenWidth = self.view.frame.width
        if let y = newsHeaderContainer?.frame.y,
            let height = newsHeaderContainer?.frame.height {
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.newsContentView?.frame = CGRect(x: 0, y: (y + height + 10), width: screenWidth, height: refreshHeight)
                let headerHeight = (self.newsContentView?.frame.y)! + (self.newsContentView?.frame.height)!
                self.headerView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: headerHeight + 1)       // +1是为了显示新闻底部分割线
                self.headerView?.isHidden = false
                self.tableView.reloadData()
                self.fetchRecommens()
                self.hiddenHud()
            })
        }
    }

    private func webViewHandleHeaderHeight(_ webView: WKWebView) {
        let js = "document.body.scrollHeight"
        webView.evaluateJavaScript(js) { [weak self] (result, error) in
            if nil == error {
                if let scrollHeight = result as? Int {
                    let height = CGFloat(scrollHeight)
                    self?.refreshHeader(withHeight: height)
                }
            }
        }
    }

    private func webViewHandleImageURLs(_ webView: WKWebView) {
        let js = "getImages()"
        webView.evaluateJavaScript(js) { [weak self] (result, error) in
            if nil == error {
                if let urls = result as? [Any] {
                    urls.forEach({ (object) in
                        let url = String(describing: object)
                        if url.hasPrefix("http://") || url.hasPrefix("https://") {
                            self?.imageURLs.append(url)
                        }
                    })
                }
            }
        }
    }

    private func showPhotoBrowser(index: Int) {

        func photoes() -> [SKPhoto] {
            var photoes = [SKPhoto]()
            imageURLs.forEach { (url) in
                let photo = SKPhoto.photoWithImageURL(url)
                photo.shouldCachePhotoURLImage = false
                photoes.append(photo)
            }
            return photoes
        }

        let browser = SKPhotoBrowser(photos: photoes())
        browser.initializePageIndex(index)
        present(browser, animated: true, completion: nil)
    }

}


// MARK: - WKNavigationDelegate Methods -
extension MNewsDetailViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        var policy: WKNavigationActionPolicy = .cancel
        if let url = navigationAction.request.url?.absoluteString {
            let about = "about:blank"
            let click = "news:imageClick:"
            if url.contains(about) {
                policy = .allow
            } else if url.contains(click) {
                let imageURL = url[click.endIndex...]
                if let clickIndex = imageURLs.firstIndex(of: String(imageURL)) {
                    showPhotoBrowser(index: clickIndex)
                }
            }
        }
        decisionHandler(policy)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        showError(error: error)
        navigationController?.popViewController(animated: true)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript(unabledTouchCalloutScript(), completionHandler: nil)
        webViewHandleHeaderHeight(webView)
        webViewHandleImageURLs(webView)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showError(error: error)
        navigationController?.popViewController(animated: true)
    }

}

