//
//  MMineService.swift
//  News
//
//  Created by RockerHX on 2018/3/23.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import RxSwift
import SwiftyJSON
import PromiseKit


struct MMineService {

    private let disposeBag = DisposeBag()

    private func fetchServerPushNewses() -> Promise<[MNews]> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.pushHistory)
                .filterSuccessfulStatusCodes()
                .mapNewses()
                .subscribe({ (event) in
                    switch event {
                    case .next(let newses):
                        result.fulfill(newses)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension MMineService {

    public func fetchPushNewses() -> Promise<[MNews]> {
        return fetchServerPushNewses()
    }

}


