//
//  MNewsViewModel.swift
//  News
//
//  Created by RockerHX on 2018/3/13.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RxSwift
import Moya


class MNewsViewModel: NSObject {

    // MARK: - Public Property -
    public var cid: String?
    public let disposeBag = DisposeBag()

    // MARK: - Private Property -
    private let service = MNewsService()
    private var listCollection = [Any]()

    // MARK: - Inputs -
    let reloadAction = PublishSubject<Void>()
    let loadMoreAction = PublishSubject<Void>()

    // MARK: - Outputs -
    public private(set) var list = PublishSubject<Result<[Any], MoyaError>>()

    // MARK: - Initialize Methods -
    override init() {
        super.init()

        setupBinding()
    }

    private func setupBinding() {
        reloadAction
            .subscribe { [weak self] _ in
                self?.reload()
            }.disposed(by: disposeBag)

        loadMoreAction
            .subscribe { [weak self] _ in
                self?.loadMore()
            }.disposed(by: disposeBag)
    }

}


extension MNewsViewModel {

    private func reload() {
        listCollection.removeAll()
        fetchNewses()
    }

    private func loadMore() {
        fetchMoreNewses()
    }

    private func fetchNewses() {
        if let channelID = cid {
            service.fetchNewses(withCID: channelID)
                .done { [weak self] newsList in
                    self?.updateCollection(newsList)
                }.catch { [weak self] (error) in
                    if let e = error as? MoyaError {
                        self?.list.onNext(.failure(e))
                    }
                }
        }
    }

    private func fetchMoreNewses() {
        if let channelID = cid, let date = (listCollection.last as? MNews)?.date {
            service.fetchMoreNewses(withCID: channelID, date: date)
                .done { [weak self] newsList in
                    self?.appendCollection(newsList)
                }.catch { [weak self] (error) in
                    if let e = error as? MoyaError {
                        self?.list.onNext(.failure(e))
                    }
                }
        }
    }

    private func updateCollection(_ collection: [Any]) {
        MAdvertPool.shared.reload { (pool) in
            let list = pool.packageList(list: collection)
            self.listCollection = collection
            self.list.onNext(.success(list))
        }
    }

    private func appendCollection(_ collection: [Any]) {
        let packageList = MAdvertPool.shared.packageList(list: collection)
        listCollection.append(contentsOf: packageList)
        list.onNext(.success(listCollection))
    }

}
