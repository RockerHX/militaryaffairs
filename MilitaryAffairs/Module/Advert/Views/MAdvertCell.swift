//
//  MAdvertCell.swift
//  News
//
//  Created by RockerHX on 2018/3/21.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import SnapKit


class MAdvertCell: UITableViewCell {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Public Methods -
    public func showAD(adView: BUNativeExpressAdView) {
        contentView.subviews.forEach { (subView) in
            subView.removeFromSuperview()
        }
        contentView.addSubview(adView)
        let height = adView.frame.height
        adView.snp.makeConstraints { (make) in
            make.height.equalTo(height)
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }

    // MARK: - Private Methods -

}

