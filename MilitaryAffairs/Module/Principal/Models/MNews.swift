//
//  MNews.swift
//  News
//
//  Created by RockerHX on 2018/3/8.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift
import DateToolsSwift


public class MNews: Codable {

    typealias State = MAppConfiguration.State

    enum `Type`: String, Codable {
        case news    = "news"
        case joke    = "joke"
        case video   = "video"
        case picture = "picture"
    }

    enum Style: String, Codable {
        case thumbnails
        case cover
        case thumbnail
        case summary
        case onlyTitle
    }

    enum Mode: String, Codable {
        case native = "NATIVE"
        case store = "STORE"
        case web = "WEBVIEW"
    }

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case source
        case url         = "sourceurl"
        case videoURL    = "video_url"
        case channelID   = "channel_id"
        case content
        case mode
        case summary
        case state       = "status"
        case type        = "ctype"
        case style
        case thumbnails  = "miniimg"
        case covers      = "lbimg"
        case duration
        case date        = "date_at"
        case localeTime
        case displayTime

        case praise
        case collect
    }

    var id: Int? = nil
    var title: String? = nil
    var source: String? = nil
    var url: String? = nil
    var videoURL: String? = nil
    var channelID: Int? = nil
    var content: String? = nil
    var mode: Mode? = nil
    var summary: String? = nil
    var state: State? = nil
    var type: Type? = nil
    var style: Style? = nil
    var thumbnails: [Image]? = nil
    var covers: [Image]? = nil
    var date: Int64? = nil
    var duration: Int? = nil
    var localeTime: String? = nil
    var displayTime: String? = nil

    var praise: String? = nil
    var collect: String? = nil

}


// MARK: - Property Struct -
extension MNews {

    struct Image: Codable {

        enum CodingKeys: String, CodingKey {
            case url = "src"
        }

        var url: String?

    }

}


// MARK: - Private Methods -
extension MNews {

    private func handleStyle() {
        if thumbnails?.count >= 3 {
            style = .thumbnails
        } else if covers?.count > 0 {
            style = .cover
        } else if thumbnails?.count > 0 {
            style = .thumbnail
        } else if nil != summary {
            style = .summary
        } else {
            style = .onlyTitle
        }
    }

    private func hanleLocaleTime() {
        if let timeStamp = date {
            let time = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
            localeTime = time.timeAgoSinceNow
        }
    }

    private func hanleDisplayTime() {
        if let timeStamp = date {
            let time = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
            displayTime = time.format(with: .full)
        }
    }

    private func cacheNews() -> MDNews? {
        if let newsID = id.string {
            let result = MDNews.fetch(withID: newsID)
            return result?.first
        }
        return nil
    }

}


// MARK: - Public Methods -
extension MNews {

    public func hanleModel() {
        handleStyle()
        hanleLocaleTime()
        hanleDisplayTime()
    }

    public func cache() {
        hanleModel()
        DispatchQueue(label: "background").async {
            autoreleasepool {
                do {
                    let realm = try Realm()
                    let data = try JSONEncoder().encode(self)
                    if let filterID = self.id,
                        let dbNews = realm.objects(MDNews.self).filter("id = '\(filterID)'").first {
                        if data != dbNews.data {
                            _ = dbNews.update {
                                dbNews.data = data
                            }
                        }
                    } else {
                        var newsJson = [String: Any]()
                        if let newsID = self.id.string, let cid = self.channelID.string {
                            newsJson["id"] = newsID
                            newsJson["cid"] = cid
                            newsJson["data"] = data
                        }
                        let news = MDNews(value: newsJson)
                        _ = news.save {}
                    }
                } catch {
                    debugPrint(error)
                }
            }
        }
    }

    @discardableResult
    public func readed() -> Bool {
        if let cacheNews = cacheNews() {
            return cacheNews.readed()
        }
        return false
    }

    @discardableResult
    public func unRead() -> Bool {
        if let cacheNews = cacheNews() {
            return cacheNews.unRead()
        }
        return false
    }

    public func favorite() -> Bool {
        if let cacheNews = cacheNews() {
            return cacheNews.favorite
        }
        return false
    }

    @discardableResult
    public func unFavorite() -> Bool {
        if let cacheNews = cacheNews() {
            return cacheNews.unFavorite()
        }
        return false
    }

    public func favorited(_ favorited: Bool) {
        if let cacheNews = cacheNews() {
            _ = cacheNews.favorited(favorited)
        }
    }

}

