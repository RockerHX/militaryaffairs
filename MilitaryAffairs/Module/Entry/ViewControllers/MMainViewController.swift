//
//  MMainViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class MMainViewController: UITabBarController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleKillAppMessage()
        if LaunchManager.manager.channelsComfirmed {
            handleMutualLogic()
        }
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        view.backgroundColor = .white   // 解决导航控制器push/pop时右边有黑影的Bug
    }
    
}


// MARK: - Public Methods -
extension MMainViewController {

    public func recevieMessageNotification(_ notification: MMessageNotification) {
        guard let style = notification.style,
            let currentNavigationContaoller = selectedViewController as? UINavigationController
            else { return }
        switch style {
        case .web:
            showSafariViewController(with: notification.url)
        case .detail:
            guard let newDetailViewController = R.storyboard.principal.mNewsDetailViewController() else { return }
            newDetailViewController.news = notification.newsObject
            currentNavigationContaoller.pushViewController(newDetailViewController, animated: true)
        }
    }

}


// MARK: - Private Methods -
extension MMainViewController {

    private func handleKillAppMessage() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let message = appDelegate?.messageNotification else { return }
        recevieMessageNotification(message)
        appDelegate?.messageNotification = nil
    }

}


import Moya
import SwiftyJSON
// MARK: - Mutual Logic -
extension MMainViewController {

    private func handleMutualLogic() {
        let provider = MoyaProvider<MApi>()
        provider.request(.mutual(appID: MAppConfigurationManager.AppID)) { [weak self] (result) in
            switch result {
            case .success(let response):
                switch response.statusCode {
                case 200:
                    self?.parseMutualModel(with: response.data)
                default:
                    self?.showHud(with: "请求出错！")
                }
            case .failure(let error):
                self?.showHud(with: error.errorDescription)
            }
        }
    }

    private func parseMutualModel(with data: Data) {
        do {
            let json = try JSON(data: data)
            let mutual: MMutual = try json["data"].rawData().decoded()
            showMutualAlert(with: mutual)
        } catch {
            debugPrint(error)
        }
    }

    static let MutualKey = "MutualKey"
    private func showMutualAlert(with mutual: MMutual) {
        let flag = UserDefaults.standard.integer(forKey: MMainViewController.MutualKey)
        if mutual.flag != flag {
            let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            cancelAction.setValue(UIColor.lightGray, forKey: "_titleTextColor")
            let enterAction = UIAlertAction(title: "免费获取", style: .default) { [weak self] (_) in
                switch mutual.mode {
                case .native:
                    break
                case .store:
                    if let url = URL(string: mutual.url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                case .web:
                    self?.showSafariViewController(with: mutual.url)
                }
            }
            showAlert(with: mutual.title, message: mutual.summary, otherActions: [cancelAction, enterAction])
        }
        UserDefaults.standard.set(mutual.flag, forKey: MMainViewController.MutualKey)
        UserDefaults.standard.synchronize()
    }

}

