//
//  PagesControlCell.swift
//  News
//
//  Created by RockerHX on 2018/3/6.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


class PagesControlCell: UICollectionViewCell, HXCellProtocol {
    
    // MARK: - IBOutlet Property -
    @IBOutlet var titleLabel: UILabel?

    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }

    // MARK: - Public Methods -
    public func display(withTitle title: String, selected: Bool) {
        titleLabel?.text = title
        titleLabel?.textColor = selected ? HXTheme.current().color : .black
    }

    // MARK: - Private Methods -
    private func configure() {
    }

}

