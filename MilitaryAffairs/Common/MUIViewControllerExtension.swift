//
//  UIViewControllerExtension.swift
//  BulletinTopLine
//
//  Created by RockerHX on 2018/4/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Moya


extension UIViewController {

    public func showError(error: Error) {
        var tip = ""
        if let e = error as? MoyaError {
            switch e {
            case .requestMapping(let message):
                tip = message
            default:
                tip = error.localizedDescription
            }
        } else {
            tip = error.localizedDescription
        }
        showHud(with: tip)
    }

}

