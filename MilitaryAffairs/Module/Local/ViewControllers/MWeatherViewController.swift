//
//  MWeatherViewController.swift
//  MilitaryAffairs
//
//  Created by RockerHX on 2018/6/21.
//Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import RxSwift
import Kingfisher


class MWeatherViewController: UIViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var cityLabel: UILabel?
    @IBOutlet var dateLabel: UILabel?
    @IBOutlet var weekLabel: UILabel?
    @IBOutlet var temperatureLabel: UILabel?
    @IBOutlet var cloudLabel: UILabel?
    @IBOutlet var pm2_5CountLabel: UILabel?
    @IBOutlet var pm2_5PromptLabel: UILabel?

    @IBOutlet var todayWeatherStateIcon: UIImageView?
    @IBOutlet var todayMinMaxLabel: UILabel?
    @IBOutlet var todayCloudLabel: UILabel?
    @IBOutlet var todayWindLabel: UILabel?

    @IBOutlet var tomorrowWeatherStateIcon: UIImageView?
    @IBOutlet var tomorrowMinMaxLabel: UILabel?
    @IBOutlet var tomorrowCloudLabel: UILabel?
    @IBOutlet var tomorrowWindLabel: UILabel?

    @IBOutlet var aftertomorrowWeatherStateIcon: UIImageView?
    @IBOutlet var aftertomorrowMinMaxLabel: UILabel?
    @IBOutlet var aftertomorrowCloudLabel: UILabel?
    @IBOutlet var aftertomorrowWindLabel: UILabel?

    // MARK: - Public Property -
    public var currentWeather: MWeather?
    public var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)

    // MARK: - Private Property -
    private let disposeBag = DisposeBag()

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        hanleWeather()
        displayCurrentWeather()
    }

    fileprivate func hanleWeather() {
        firstly {
            self.fetchWeathers(whti: coordinate)
        }.done { (weathers) in
            self.display(with: weathers)
        }.catch { (error) in
            self.showHud(with: error.localizedDescription)
        }
    }

    fileprivate func displayCurrentWeather() {
        temperatureLabel?.text = currentWeather?.temperature
        dateLabel?.text = currentWeather?.date
        weekLabel?.text = currentWeather?.week
        cloudLabel?.text = currentWeather?.cloud
        cityLabel?.text = currentWeather?.city
    }

    fileprivate func display(with weathers: [MWeather]) {
        if 3 == weathers.count {
            displayTodayWeather(with: weathers[0])
            displayTomorrowWeather(with: weathers[1])
            displayAferTomorrowWeather(with: weathers[2])
        }
    }

    fileprivate func displayTodayWeather(with weather: MWeather) {
        if let iconURL = weather.icon {
            todayWeatherStateIcon?.kf.setImage(with: URL(string: iconURL))
        }
        todayMinMaxLabel?.text = "\(weather.min ?? "--")°/\(weather.max ?? "--")°"
        todayCloudLabel?.text = weather.text
        todayWindLabel?.text = "\(weather.windDirection ?? "--")  \(weather.windDirectionDegree ?? "--")°"
    }

    fileprivate func displayTomorrowWeather(with weather: MWeather) {
        if let iconURL = weather.icon {
            tomorrowWeatherStateIcon?.kf.setImage(with: URL(string: iconURL))
        }
        tomorrowMinMaxLabel?.text = "\(weather.min ?? "--")°/\(weather.max ?? "--")°"
        tomorrowCloudLabel?.text = weather.text
        tomorrowWindLabel?.text = "\(weather.windDirection ?? "--")  \(weather.windDirectionDegree ?? "--")°"
    }

    fileprivate func displayAferTomorrowWeather(with weather: MWeather) {
        if let iconURL = weather.icon {
            aftertomorrowWeatherStateIcon?.kf.setImage(with: URL(string: iconURL))
        }
        aftertomorrowMinMaxLabel?.text = "\(weather.min ?? "--")°/\(weather.max ?? "--")°"
        aftertomorrowCloudLabel?.text = weather.text
        aftertomorrowWindLabel?.text = "\(weather.windDirection ?? "--")  \(weather.windDirectionDegree ?? "--")°"
    }
    
}


import PromiseKit
import Moya
import CoreLocation
// MARK: - Private Methods Logic Methods -
extension MWeatherViewController {

    fileprivate func fetchWeathers(whti coordinate: CLLocationCoordinate2D) -> Promise<[MWeather]> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.weathers(coordinate: coordinate))
                .filterSuccessfulStatusCodes()
                .mapWeathers()
                .subscribe({ (event) in
                    switch event {
                    case .next(let weather):
                        result.fulfill(weather)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    fileprivate func mapWeathers() -> Observable<[MWeather]> {
        return flatMap { response -> Single<[MWeather]> in
            return Single.just(try response.mapWeathers())
        }.asObservable()
    }

}


import SwiftyJSON
extension Response {

    fileprivate func mapWeathers() throws -> [MWeather] {
        do {
            var weathers = [MWeather]()
            let json = try JSON(data: data)
            let code = json["code"].intValue
            let tip = json["tip"].stringValue
            if 0 == code {
                guard let jsons = json["data"].dictionary?["daily"]?.arrayValue else { return weathers}
                for json in jsons {
                    let weather = try JSONDecoder().decode(MWeather.self, from: json.rawData())
                    weathers.append(weather)
                }
            } else {
                throw MoyaError.requestMapping(tip)
            }
            return weathers
        } catch {
            throw error
        }
    }

}

