//
//  MVideoViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


enum MVideoSegue: String, HXSegue {

    typealias Element = UIViewController

    case Video2Channel  = "Video2Channel"
    case List2Detial    = "List2Detial"
}


class MVideoViewController: UIViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    // MARK: - Private Property -
    private var pagesViewController: PagesViewController?

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if MVideoSegue.Video2Channel.identifier == identifier {
                let channelViewController = (segue.destination as? UINavigationController)?.viewControllers.first as? MChannelViewController
                channelViewController?.delegate = self
                channelViewController?.type = .video
            } else if String(describing: PagesViewController.self) == identifier {
                pagesViewController = segue.destination as? PagesViewController
            }
        }
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    // MARK: - Private Methods -
    private func configure() {
        fetchAvailableChannels()
    }

    private func fetchAvailableChannels() {
        if let channels = MDChannel.fetchAvailableVideoChannels() {
            var titles = [String]()
            var viewControllers = [UIViewController]()
            channels.forEach({ (channel) in
                titles.append(channel.name!)
                let controller = MVideoListViewController.instance()
                controller.category = channel.name
                viewControllers.append(controller)
            })
            pagesViewController?.titles = titles
            pagesViewController?.viewControllers = viewControllers
            pagesViewController?.reloadData()
        }
    }

    private func channelsReload() {
        fetchAvailableChannels()
    }
    
}


// MARK: - MChannelViewControllerDelegate -
extension MVideoViewController: MChannelViewControllerDelegate {

    func channelsComfirm() {
        channelsReload()
    }
}

