# Uncomment the next line to define a global platform for your project
platform :ios, '12.2'

target 'Military' do
  # Comment the next line if you're not using Swift and don't want to use dynamic frameworks
  use_frameworks!
  inhibit_all_warnings!

  # ---- Database ----
  pod 'RealmSwift', '~> 10.7.3'

  # ---- Networking ----
  pod 'Alamofire', '~> 5.4.3'
  pod 'Moya/RxSwift', '~> 14.0.0'

  # ---- Cache ----
  pod 'Kingfisher', '~> 6.3.0'

  # ---- JSON ----
  pod 'SwiftyJSON', '~> 5.0.1'

  # ---- Utility ----
  pod 'DateToolsSwift', '~> 5.0.0'
  pod 'SKPhotoBrowser', '~> 6.1.0'
  pod 'ZFPlayer', '~> 4.0.1'
  pod 'ZFPlayer/AVPlayer', '~> 4.0.1'
  pod 'ZFPlayer/ControlView', '~> 4.0.1'
  pod 'Devices', '~> 1.1.0'
  pod 'ReachabilitySwift', '~> 5.0.0'
  pod 'R.swift', '~> 5.4.0'

  # ---- Kit ----
  pod 'RxCocoa', '~> 5.1.1'
  pod 'RxSwift', '~> 5.1.2'
  pod 'PromiseKit', '~> 6.13.1'
  pod 'PromiseKit/CoreLocation'
  pod 'SnapKit', '~> 5.0.1'

  # ---- UI ----
  pod 'MBProgressHUD', '~> 1.2.0'
  pod 'CRRefresh', '~> 1.1.3'

  # ---- SDK ----
  pod 'Ads-CN-Beta'

  pod 'UMCommon'
  pod 'UMCSecurityPlugins'
  pod 'UMDevice'

  pod 'UMPush'

  pod 'UMShare/UI', :inhibit_warnings => true
  pod 'UMShare/Social/WeChat', :inhibit_warnings => true
  pod 'UMShare/Social/QQ', :inhibit_warnings => true

end

post_install do |installer|
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '12.0'
            config.build_settings['LD_NO_PIE'] = 'NO'
        end
    end
end
