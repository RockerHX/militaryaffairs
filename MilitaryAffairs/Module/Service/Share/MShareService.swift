//
//  MShareService.swift
//  快报头条
//
//  Created by RockerHX on 2018/3/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


struct MShareService {

    static public func shareApp() {
        let appName = Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
        let prompt = """
                        致力于打造最权威、新闻资讯最快、功能最强的军事类应用,为所有军事爱好者,提供更优质、便捷的军事新闻资讯和互动服务体验。
                        让你随时随地掌握全球军事、国际动态,运筹帷幄！

                        【产品特色】
                        - 聚焦军情：为您聚焦全球热点军事资讯；重大、突发新闻推送，秒发到您手中
                        - 视频：集各种分类视频于一身，让您随时随地享看不停。
                        - 海量信息资讯，有量且有质量，更新迅速来源站权威
                        - 最先进算法，完全个性化订阅，颠覆你的阅读体验，只看想看的内容
                        - 界面清爽简洁，字体可调节，看新闻更方便
                        - 支持离线收藏
                        - 微博微信QQ空间随意分享
                     """
        let appstoreURL = "https://itunes.apple.com/cn/app/军事头条/id1271673198?l=zh&ls=1&mt=8"
        share(withTitle: appName, prompt: prompt, url: appstoreURL)
    }

    static public func share(withTitle title: String, prompt: String, url: String, thumImage: Any? = "https://is4-ssl.mzstatic.com/image/thumb/Purple118/v4/d1/12/4c/d1124c8a-ee1d-8abb-f30b-a9fbfea82614/AppIcon-1x_U007emarketing-85-220-1.png/246x0w.jpg") {
        UMSocialUIManager.showShareMenuViewInWindow { (platform, userInfo) in
            let message = UMSocialMessageObject()
            let shareObject = UMShareWebpageObject.shareObject(withTitle: title, descr: prompt, thumImage: thumImage)
            shareObject?.webpageUrl = url
            message.shareObject = shareObject
            let controller = UIApplication.shared.delegate?.window??.rootViewController
            UMSocialManager.default().share(to: platform, messageObject: message, currentViewController: controller!) { (data, error) in
                if let e = error {
                    debugPrint(e)
                } else {
                    if let response = data as? UMSocialShareResponse {
                        debugPrint(response.message ?? "")
                        debugPrint(response.originalResponse ?? "")
                    }
                }
            }
        }
    }

}

