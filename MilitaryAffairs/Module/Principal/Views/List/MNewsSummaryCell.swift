//
//  MNewsSummaryCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


class MNewsSummaryCell: MNewsCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var summaryLabel: UILabel?

    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Public Methods -
    override func display(withNews news: MNews) {
        super.display(withNews: news)
        summaryLabel?.text = news.summary
    }

    // MARK: - Private Methods -

}

