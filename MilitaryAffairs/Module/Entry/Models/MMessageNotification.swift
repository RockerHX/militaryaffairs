//
//  MMessageNotification.swift
//
//  Created by RockerHX on 2018/9/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Foundation


extension MMessageNotification {

    enum Style: String, Codable {
        case web    = "web"
        case detail = "detail"
    }

}


class MMessageNotification: Codable {

    enum CodingKeys: String, CodingKey {
        case style = "type"
        case url
        case newsData = "news"
        case newsObject
    }

    var style: Style? = nil
    var url: String? = nil
    var newsData: String? = nil
    var newsObject: MNews? = nil

}


extension MMessageNotification {

    public func parseObject() {
        guard let data = newsData?.data(using: .utf8) else { return }
        do {
            newsObject = try data.decoded() as MNews
        } catch {
            debugPrint(error)
        }
    }

}

