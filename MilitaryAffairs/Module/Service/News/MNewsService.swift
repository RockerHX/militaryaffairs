//
//  MNewsService.swift
//  News
//
//  Created by RockerHX on 2018/3/8.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import RxSwift
import SwiftyJSON
import PromiseKit


struct MNewsService {

    private let disposeBag = DisposeBag()

    private func fetchCacheNewses(withCID cid: String) -> Promise<[MNews]> {
        var newses = [MNews]()
        if let cacheNewses = MDNews.fetch(withChannelID: cid) {
            cacheNewses.forEach({ (cache) in
                if let data = cache.data {
                    if let news = try? JSONDecoder().decode(MNews.self, from: data) {
                        news.hanleModel()
                        newses.append(news)
                    }
                }
            })
        }
        if newses.count < 10 {
            newses.removeAll()
        }
        return Promise{ result in
            let sorted = newses.sorted(by: { $0.date! > $1.date! })
            result.fulfill(sorted)
        }
    }

    private func fetchServerNewses(withCID cid: String, cache: [MNews] = [MNews]()) -> Promise<[MNews]> {
        var date: Int64?
        if !cache.isEmpty {
            date = cache.first?.date
        }
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.newses(cid: cid, date: date, action: .refresh))
                .filterSuccessfulStatusCodes()
                .mapNewses()
                .subscribe({ (event) in
                    switch event {
                    case .next(var newses):
                        newses.append(contentsOf: cache)
                        result.fulfill(newses)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

    private func fetchServerMoreNewses(withCID cid: String, date: Int64) -> Promise<[MNews]> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.newses(cid: cid, date: date, action: .page))
                .filterSuccessfulStatusCodes()
                .mapNewses()
                .subscribe({ (event) in
                    switch event {
                    case .next(let newses):
                        result.fulfill(newses)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

    private func fetchServerNewsRecommends(withNewsID id: String) -> Promise<[MNews]> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.newsRecommends(id: id))
                .filterSuccessfulStatusCodes()
                .mapNewses()
                .subscribe({ (event) in
                    switch event {
                    case .next(let recommends):
                        result.fulfill(recommends)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension MNewsService {

    public func fetchNewses(withCID cid: String) -> Promise<[MNews]> {
        return fetchCacheNewses(withCID: cid)
                .then { cacheNewses in
                    self.fetchServerNewses(withCID: cid, cache: cacheNewses)
                }
    }

    public func fetchMoreNewses(withCID cid: String, date: Int64) -> Promise<[MNews]> {
        return fetchServerMoreNewses(withCID: cid, date: date)
    }

    public func fetchNewsRecommends(withNewsID id: String) -> Promise<[MNews]> {
        return fetchServerNewsRecommends(withNewsID: id)
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    func mapNewses() -> Observable<[MNews]> {
        return flatMap { response -> Single<[MNews]> in
            return Single.just(try response.mapNewses())
        }.asObservable()
    }
    
}


extension Response {

    fileprivate func mapNewses() throws -> [MNews] {
        do {
            var newses = [MNews]()
            let json = try JSON(data: data)
            let code = json["code"].intValue
            let tip = json["tip"].stringValue
            if 0 == code {
                let jsons = json["data"].arrayValue
                for json in jsons {
                    let news = try JSONDecoder().decode(MNews.self, from: json.rawData())
                    news.cache()
                    newses.append(news)
                }
            } else {
                throw MoyaError.requestMapping(tip)
            }
            if newses.count < 10 {
                MoyaProvider<MApi>().request(.log(count: newses.count, url: "\((request?.url)!)", type: "news", channel: "")) {_ in }
            }
            return newses
        } catch {
            throw error
        }
    }

}


extension MNewsService {

    public func fetchNewsDetailContent(withID id: String) -> Promise<String> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.newsDetail(id: id))
                .filterSuccessfulStatusCodes()
                .mapDetail()
                .subscribe({ (event) in
                    switch event {
                    case .next(let news):
                        result.fulfill(news)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    fileprivate func mapDetail() -> Observable<String> {
        return flatMap { response -> Single<String> in
            return Single.just(try response.mapDetail())
            }.asObservable()
    }

}


extension Response {

    fileprivate func mapDetail() throws -> String {
        do {
            var content = ""
            let json = try JSON(data: data)
            let code = json["code"].intValue
            let tip = json["tip"].stringValue
            if 0 == code {
                let jsons = json["data"].dictionaryValue
                if let html = jsons["content"]?.stringValue {
                    content = html
                }
            } else {
                throw MoyaError.requestMapping(tip)
            }
            return content
        } catch {
            throw error
        }
    }

}

