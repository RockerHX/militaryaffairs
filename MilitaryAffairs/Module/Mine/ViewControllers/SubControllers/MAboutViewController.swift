//
//  MAboutViewController.swift
//  BulletinTopLine
//
//  Created by RockerHX on 2018/4/10.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class MAboutViewController: UIViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var versionLabel: UILabel?
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        if let infoDictionary = Bundle.main.infoDictionary,
            let version = infoDictionary["CFBundleShortVersionString"] as? String,
            let build = infoDictionary["CFBundleVersion"] as? String {
            versionLabel?.text = "Version: \(version) (Build \(build))"
        }
    }
    
}

