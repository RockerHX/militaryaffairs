//
//  NWebViewController.swift
//  Longevity
//
//  Created by RockerHX on 2019/1/17.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import WebKit
import SnapKit


class NWebViewController: UIViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var url: String?

    // MARK: - Private Property -
    private var contentView: WKWebView?

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }

    // MARK: - Override Methods -
    // MARK: - Configuration Methods -
    private func configure() {
        showHud()
        contentView = setupContentView()
        guard let source = url, let requestURL = URL(string: source) else { return }
        contentView?.load(URLRequest(url: requestURL))
    }

    private func setupContentView() -> WKWebView {
        let webCofiguration = WKWebViewConfiguration()
        let rect = CGRect.zero
        let webView = WKWebView(frame: rect, configuration: webCofiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view.addSubview(webView)
        webView.snp.makeConstraints { [weak self] (maker) in
            guard let this = self else { return }
            maker.top.equalTo(this.view)
            maker.bottom.equalTo(this.view)
            maker.left.equalTo(this.view)
            maker.right.equalTo(this.view)
        }
        return webView
    }

}


// MARK: - Event Methods -
extension NWebViewController {
}


// MARK: - Public Methods -
extension NWebViewController {
}


// MARK: - Private Methods -
extension NWebViewController {
}


// MARK: - WKUIDelegate Methods -
extension NWebViewController: WKUIDelegate {

    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let action = UIAlertAction(title: "确认", style: .default) { (_) in
            completionHandler()
        }
        showAlert(with: "提示", message: message, otherActions: [action])
    }

    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let enterAction = UIAlertAction(title: "确认", style: .default) { (_) in
            completionHandler(true)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default) { (_) in
            completionHandler(false)
        }
        showAlert(with: "提示", message: message, otherActions: [enterAction, cancelAction])
    }

    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: prompt, message: nil, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        let action = UIAlertAction(title: "完成", style: .default) { (_) in
            completionHandler(alertController.textFields?.first?.text ?? "")
        }
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }

}


// MARK: - WKNavigationDelegate Methods -
extension NWebViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hiddenHud()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hiddenHud()
        showError(error: error)
        navigationController?.popViewController(animated: true)
    }

}

