//
//  MLocal.swift
//  MilitaryAffairs
//
//  Created by RockerHX on 2018/6/22.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import DateToolsSwift


public class MLocal: Codable {

    enum `Type`: String, Codable {
        case news    = "news"
        case joke    = "joke"
        case video   = "video"
        case picture = "picture"
    }

    enum Style: String, Codable {
        case thumbnails
        case thumbnail
        case onlyTitle
    }

    enum CodingKeys: String, CodingKey {
        case id          = "docid"
        case title
        case source
        case url
        case shareURL    = "share_url"
        case date
        case type        = "ctype"

        case style
        case thumbnails  = "images"

        case localeTime
        case displayTime
    }

    var id: String? = nil
    var title: String? = nil
    var source: String? = nil
    var url: String? = nil
    var shareURL: String? = nil
    var date: String? = nil
    var type: Type? = nil

    var style: Style? = nil
    var thumbnails: [String]? = nil

    var localeTime: String? = nil
    var displayTime: String? = nil

}


// MARK: - Private Methods -
extension MLocal {

    private func handleStyle() {
        guard let count = thumbnails?.count else {
            style = .onlyTitle
            return
        }
        switch count {
        case 1:
            style = .thumbnail
        case 3:
            style = .thumbnails
        default:
            return
        }
    }

    private func hanleLocaleTime() {
        if let d = date {
            let time = Date(dateString: d, format: "yyyy-MM-dd hh:mm:ss")
            localeTime = time.timeAgoSinceNow
        }
    }

}


// MARK: - Public Methods -
extension MLocal {

    public func hanleModel() {
        handleStyle()
        hanleLocaleTime()
    }

}

