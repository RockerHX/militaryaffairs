//
//  MLocationViewController.swift
//  MilitaryAffairs
//
//  Created by RockerHX on 2018/6/21.
//Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import RxSwift
import Kingfisher


enum NLocationSegue: String, HXSegue {

    typealias Element = MLocationViewController

    case Location2Weathers  = "Location2Weathers"
    case Location2Operation = "Location2Operation"
    case Location2Local     = "Location2Local"
}


class MLocationViewController: UIViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var temperatureLabel: UILabel?
    @IBOutlet var minMaxLabel: UILabel?
    @IBOutlet var dateLabel: UILabel?
    @IBOutlet var weekLabel: UILabel?
    @IBOutlet var pm2_5CountLabel: UILabel?
    @IBOutlet var pm2_5PromptLabel: UILabel?
    @IBOutlet var weatherStateIcon: UIImageView?
    @IBOutlet var cloudLabel: UILabel?
    @IBOutlet var windStateLabel: UILabel?
    @IBOutlet var cityLabel: UILabel?

    @IBOutlet var operationLabel: UILabel?

    // MARK: - Public Property -
    public var snapshotView = UIView()

    // MARK: - Private Property -
    private let disposeBag = DisposeBag()
    private var locationCoordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    public var currentWeather: MWeather?

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        if NLocationSegue.Location2Weathers.identifier == identifier {
            let weatherViewController = segue.destination as? MWeatherViewController
            weatherViewController?.currentWeather = currentWeather
            weatherViewController?.coordinate = locationCoordinate
        } else if NLocationSegue.Location2Operation.identifier == identifier {
            let listViewController = segue.destination as? MPNewsListViewController
            listViewController?.id = "\(currentWeather?.id ?? 0)"
            listViewController?.title = MAppConfigurationManager.manager.configuration?.locationName
        } else if NLocationSegue.Location2Local.identifier == identifier {
            let localListViewController = segue.destination as? MLocalListViewController
            localListViewController?.coordinate = locationCoordinate
        }
    }

    // MARK: - Event Methods -
    @IBAction func refreshTaped() {
        hanleWeather()
    }

    @IBAction func operationButtonTaped() {
        let configuration = MAppConfigurationManager.manager.configuration
        guard let state = configuration?.locationState else { return }
        switch state {
        case .operation:
            NLocationSegue.Location2Operation.performSegue(on: self)
        case .local:
            NLocationSegue.Location2Local.performSegue(on: self)
        default:
            return
        }
    }

    @IBAction func sendEmailButtonTaped() {
        sendEmail()
    }

    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        handleBlurEffect()
        hanleWeather()
        handleOperation()
    }

    fileprivate func handleOperation() {
        let configuration = MAppConfigurationManager.manager.configuration
        guard let state = configuration?.locationState else { return }
        switch state {
        case .operation:
            operationLabel?.text = configuration?.locationName
        default:
            return
        }
    }

    fileprivate func handleBlurEffect() {
        let blurEffect = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: blurEffect)
        effectView.frame = snapshotView.bounds
        snapshotView.addSubview(effectView)
        view.insertSubview(snapshotView, at: 0)
    }

    fileprivate func hanleWeather() {
        showHud()
        firstly {
            fetchLocation()
        }.then { (coordinate) in
            self.fetchCurrentWeather(whti: coordinate)
        }.done { (weather) in
            self.display(with: weather)
        }.catch { (error) in
            self.hiddenHud()
            self.showHud(with: error.localizedDescription)
        }
    }

    fileprivate func display(with weather: MWeather) {
        hiddenHud()
        currentWeather = weather
        if let iconURL = weather.bigIcon {
            weatherStateIcon?.kf.setImage(with: URL(string: iconURL))
        }
        temperatureLabel?.text = weather.temperature
        dateLabel?.text = weather.date
        weekLabel?.text = weather.week
        cloudLabel?.text = weather.cloud
        cityLabel?.text = weather.city
    }

}


import PromiseKit
import Moya
import CoreLocation
// MARK: - Private Methods Logic Methods -
extension MLocationViewController {

    fileprivate func fetchLocation() -> Promise<CLLocationCoordinate2D> {
        return MLocationService.service.requestLocation()
            .then { (locations) -> Promise<CLLocationCoordinate2D> in
                let coordinate = (locations.last?.coordinate)!
                return Promise { $0.fulfill(coordinate) }
            }
    }

    fileprivate func fetchCurrentWeather(whti coordinate: CLLocationCoordinate2D) -> Promise<MWeather> {
        locationCoordinate = coordinate
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.weather(coordinate: coordinate))
                .filterSuccessfulStatusCodes()
                .mapWeather()
                .subscribe({ (event) in
                    switch event {
                    case .next(let weather):
                        result.fulfill(weather)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


import MessageUI
import Devices
// MARK: - MFMailComposeViewControllerDelegate Methods -
extension MLocationViewController: MFMailComposeViewControllerDelegate {

    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let emailContoller = MFMailComposeViewController()
            emailContoller.mailComposeDelegate = self
            emailContoller.setToRecipients(["3030923451@qq.com"])
            emailContoller.setSubject("应用-\(Device.Application.shared.name) 问题反馈")
            present(emailContoller, animated: true, completion: nil)
        } else {
            showHud(with: "请先到设置里添加邮件账户")
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        guard let e = error else { return }
        showHud(with: e.localizedDescription)
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    fileprivate func mapWeather() -> Observable<MWeather> {
        return flatMap { response -> Single<MWeather> in
            return Single.just(try response.mapWeather())
        }.asObservable()
    }

}


import SwiftyJSON
extension Response {

    fileprivate func mapWeather() throws -> MWeather {
        do {
            let json = try JSON(data: data)
            let code = json["code"].intValue
            let tip = json["tip"].stringValue
            if 0 == code {
                let weather = try JSONDecoder().decode(MWeather.self, from: json["data"].rawData())
                return weather
            } else {
                throw MoyaError.requestMapping(tip)
            }
        } catch {
            throw error
        }
    }

}
