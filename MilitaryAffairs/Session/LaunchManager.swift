//
//  LaunchManager.swift
//  unWaste
//
//  Created by RockerHX on 2017/8/12.
//  Copyright © 2017年 RockerHX. All rights reserved.
//


import UIKit
import RealmSwift
import Reachability


class LaunchManager {

    enum Key: String {
        case FirstLanuch        = "LaunchManager.NotFirstLanuch"
        case ChannelsComfirmed  = "LaunchManager.ChannelsComfirmed"
    }

    // MARK: - Singleton -
    static let manager = LaunchManager()

    // MARK: - Public Property -
    public private(set) var firstLanuch = true
    public private(set) var channelsComfirmed = false

    // MARK: - Privaet Property -
    let reachability = try? Reachability()

    // MARK: - Initialize Methods -
    fileprivate init() {
        initializeConfigure()
        initializeRealmData()
        configureReachability()
    }

    // MARK: - Public Methods -
    public func channelscomfirm() {
        channelsComfirmed = true
        UserDefaults.standard.set(true, forKey: Key.ChannelsComfirmed.rawValue)
    }

    // MARK: - Private Methods -
    fileprivate func configureReachability() {
        reachability?.whenUnreachable = { _ in
            print("Not reachable")
        }
        do {
            try reachability?.startNotifier()
        } catch {
            debugPrint("Unable to start notifier")
        }
    }

    fileprivate func initializeConfigure() {
        let color = UIColor.color(with: "#2B85F5")
        HXTheme.apply(whtiTheme: .custom(with: color))

        let key = Key.FirstLanuch.rawValue
        let userDefaults = UserDefaults.standard
        if let infoDictionary = Bundle.main.infoDictionary,
           let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
            if let storeVersion = userDefaults.string(forKey: key) {
                firstLanuch = (currentVersion != storeVersion)
            }
            userDefaults.set(currentVersion, forKey: key)
            userDefaults.synchronize()
        }

        channelsComfirmed = UserDefaults.standard.bool(forKey: Key.ChannelsComfirmed.rawValue)
    }

    fileprivate func initializeRealmData() {
        migrationRealm()
        initializeChannelsInRealm()
    }

    fileprivate func migrationRealm() {
        // 此段代码位于 application(application:didFinishLaunchingWithOptions:)

        let config = Realm.Configuration(
            // 设置新的架构版本。必须大于之前所使用的
            // （如果之前从未设置过架构版本，那么当前的架构版本为 0）
            schemaVersion: 1,

            // 设置模块，如果 Realm 的架构版本低于上面所定义的版本，
            // 那么这段代码就会自动调用
            migrationBlock: { migration, oldSchemaVersion in
                // 我们目前还未执行过迁移，因此 oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // 没有什么要做的！
                    // Realm 会自行检测新增和被移除的属性
                    // 然后会自动更新磁盘上的架构
                }
        })

        // 通知 Realm 为默认的 Realm 数据库使用这个新的配置对象
        Realm.Configuration.defaultConfiguration = config

        // 现在我们已经通知了 Realm 如何处理架构变化，
        // 打开文件将会自动执行迁移
        _ = try? Realm()
    }

    fileprivate func loadBundleResources(forResource: String?, withExtension: String?) -> Any? {
        if let url = Bundle.main.url(forResource: forResource, withExtension: withExtension), let data = try? Data(contentsOf: url) {
            if let result = try? PropertyListSerialization.propertyList(from: data, format: nil) {
                return result
            }
        }
        return nil
    }

    fileprivate func initializeChannelsInRealm() {
        if let items = loadBundleResources(forResource: "Channels", withExtension: "plist") as? [[String: String]] {
            for item in items {
                MChannel.cache(object: item)
            }
        }
    }

}

