//
//  MChannelCell.swift
//  News
//
//  Created by RockerHX on 2018/3/1.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


class MChannelCell: UICollectionViewCell {
    
    // MARK: - IBOutlet Property -
    @IBOutlet var selectedComfirmButton: UIButton!
    @IBOutlet var nameLabel: UILabel!

    // MARK: - Public Property -
    override var isSelected: Bool {
        willSet {
            selectedComfirmButton.isSelected = !selectedComfirmButton.isSelected
        }
    }

    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        configureUI()
    }

    // MARK: - Public Methods -
    public func display(withChannel channel: MDChannel) {
        selectedComfirmButton.isSelected = channel.select
        nameLabel.text = channel.name
        if channel.name?.count == 4 {
            nameLabel.font = UIFont.systemFont(ofSize: nameLabel.font.pointSize - 1)
        } else if channel.name?.count > 4 {
            nameLabel.font = UIFont.systemFont(ofSize: nameLabel.font.pointSize - 3)
        }
    }

    // MARK: - Private Methods -
    private func configureUI() {
        
        layer.cornerRadius = 4.0
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.gray.cgColor
    }
    
}

