//
//  MVideoService.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import RxSwift
import SwiftyJSON
import PromiseKit


struct MVideoService {

    private let disposeBag = DisposeBag()

    private func fetchCacheVideos(withCategory category: String) -> Promise<[MVideo]> {
        var videos = [MVideo]()
        if let cacheVideos = MDVideo.fetch(withCategory: category) {
            cacheVideos.forEach({ (cache) in
                if let data = cache.data {
                    if let video = try? JSONDecoder().decode(MVideo.self, from: data) {
                        video.handleModel()
                        videos.append(video)
                    }
                }
            })
        }
        if videos.count < 10 {
            videos.removeAll()
        }
        return Promise { result in
            let sorted = videos.sorted(by: { $0.date! > $1.date! })
            result.fulfill(sorted)
        }
    }

    private func fetchServerVideos(withCategory category: String, cache: [MVideo] = [MVideo]()) -> Promise<[MVideo]> {
        var date: Int64?
        if !cache.isEmpty {
            date = cache.first?.date
        }
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.videos(category: category, date: date, action: .refresh))
                .filterSuccessfulStatusCodes()
                .mapVideos()
                .subscribe({ (event) in
                    switch event {
                    case .next(var videos):
                        videos.append(contentsOf: cache)
                        result.fulfill(videos)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

    private func fetchServerMoreVideos(withCategory category: String, date: Int64) -> Promise<[MVideo]> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.videos(category: category, date: date, action: .page))
                .filterSuccessfulStatusCodes()
                .mapVideos()
                .subscribe({ (event) in
                    switch event {
                    case .next(let videos):
                        result.fulfill(videos)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

    private func fetchServerVideoRecommends(withVideoID id: String) -> Promise<[MVideo]> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.videoRecommends(id: id))
                .filterSuccessfulStatusCodes()
                .mapVideos()
                .subscribe({ (event) in
                    switch event {
                    case .next(let recommends):
                        result.fulfill(recommends)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension MVideoService {

    public func fetchVideos(withCategory category: String) -> Promise<[MVideo]> {
        return fetchCacheVideos(withCategory: category)
            .then { cacheVideos in
                self.fetchServerVideos(withCategory: category, cache: cacheVideos)
        }
    }

    public func fetchMoreVideos(withCategory category: String, date: Int64) -> Promise<[MVideo]> {
        return fetchServerMoreVideos(withCategory: category, date: date)
    }

    public func fetchVideoRecommends(withVideoID id: String) -> Promise<[MVideo]> {
        return fetchServerVideoRecommends(withVideoID: id)
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    fileprivate func mapVideos() -> Observable<[MVideo]> {
        return flatMap { response -> Single<[MVideo]> in
            return Single.just(try response.mapVideos())
            }.asObservable()
    }

}


extension Response {

    fileprivate func mapVideos() throws -> [MVideo] {
        do {
            var videos = [MVideo]()
            let responseJSON = try JSON(data: data)
            let code = responseJSON["code"].intValue
            let tip = responseJSON["tip"].stringValue
            if 0 == code {
                let jsons = responseJSON["data"].arrayValue
                for json in jsons {
                    let video = try JSONDecoder().decode(MVideo.self, from: json.rawData())
                    video.cache()
                    videos.append(video)
                }
            } else {
                throw MoyaError.requestMapping(tip)
            }
            if videos.count < 10 {
                MoyaProvider<MApi>().request(.log(count: videos.count, url: "\((request?.url)!)", type: "video", channel: "")) {_ in }
            }
            return videos
        } catch {
            throw error
        }
    }

}

