//
//  MVideo.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift
import DateToolsSwift


public class MVideo: Codable {

    enum Mode: String, Codable {
        case native = "NATIVE"
        case store = "STORE"
        case web = "WEBVIEW"
    }

    enum CodingKeys: String, CodingKey {
        case id
        case title       = "video_title"
        case summary     = "video_summary"
        case url         = "video_url"
        case h5URL       = "html5_url"
        case authors
        case date        = "publish_time"
        case coverURL    = "video_image"
        case duration    = "video_duration"
        case category
        case localeTime
        case clicks      = "click_no"

        case praise
        case collect
        case prompt      = "intro"
        case mode
    }

    var id: Int? = nil
    var title: String? = nil
    var summary: String? = nil
    var url: String? = nil
    var h5URL: String? = nil
    var authors: String? = nil
    var date: Int64? = nil
    var coverURL: String? = nil
    var duration: String? = nil
    var category: String? = nil
    var localeTime: String? = nil
    var clicks: Int? = nil
    var clickPrompt: String? = nil

    var praise: String? = nil
    var collect: String? = nil
    var prompt: String? = nil
    var mode: Mode? = nil

}


// MARK: - Private Methods -
extension MVideo {

    private func hanleLocaleTime() {
        if let timeStamp = date {
            let time = Date(timeIntervalSince1970: TimeInterval(timeStamp/1000))
            localeTime = time.timeAgoSinceNow
        }
    }

    private func hanleClickCount() {
        if let count = clicks {
            switch count {
            case 0...9999:
                clickPrompt = "\(count)次播放"
            case 10000...Int.max:
                clickPrompt = "\(count/10000)万次播放"
            default:
                clickPrompt = ""
            }
        }
    }

    private func cacheVideo() -> MDVideo? {
        if let videoID = id.string {
            let result = MDVideo.fetch(withID: videoID)
            return result?.first
        }
        return nil
    }

}


// MARK: - Public Methods -
extension MVideo {

    public func handleModel() {
        hanleLocaleTime()
        hanleClickCount()
    }

    public func cache() {
        hanleLocaleTime()
        hanleClickCount()
        DispatchQueue(label: "background").async {
            autoreleasepool {
                do {
                    let realm = try Realm()
                    let data = try JSONEncoder().encode(self)
                    if let filterID = self.id,
                        let dbVideo = realm.objects(MDVideo.self).filter("id = '\(filterID)'").first {
                        if data != dbVideo.data {
                            _ = dbVideo.update {
                                dbVideo.data = data
                            }
                        }
                    } else {
                        var videoJson = [String: Any]()
                        if let videoID = self.id.string, let videoCategory = self.category {
                            videoJson["id"] = videoID
                            videoJson["category"] = videoCategory
                            videoJson["data"] = data
                        }
                        let video = MDVideo(value: videoJson)
                        _ = video.save {}
                    }
                } catch {
                    debugPrint(error)
                }
            }
        }
    }

    @discardableResult
    public func readed() -> Bool {
        if let cacheVideo = cacheVideo() {
            return cacheVideo.readed()
        }
        return false
    }

    @discardableResult
    public func unRead() -> Bool {
        if let cacheVideo = cacheVideo() {
            return cacheVideo.unRead()
        }
        return false
    }

    public func favorite() -> Bool {
        if let cacheVideo = cacheVideo() {
            return cacheVideo.favorite
        }
        return false
    }

    @discardableResult
    public func unFavorite() -> Bool {
        if let cacheVideo = cacheVideo() {
            return cacheVideo.unFavorite()
        }
        return false
    }

    public func favorited(_ favorited: Bool) {
        if let cacheVideo = cacheVideo() {
            _ = cacheVideo.favorited(favorited)
        }
    }

}

