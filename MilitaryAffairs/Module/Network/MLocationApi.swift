//
//  MLocationApi.swift
//  MilitaryAffairs
//
//  Created by RockerHX on 2018/5/26.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import CoreLocation


enum YiDianZiXun: String {
    case appID = "hLT5WPCH9xr5l8SQWeFjAw6d"
    case appKey = "cJ9cGSnF7NqYNkE9doQlF9UVYexlVgal"
}


enum MLocationApi {
    case list(offset: Int, count: Int, coordinate: CLLocationCoordinate2D)

    private func secretkey(timestamp: Int, nonce: String) -> String {
        let appKey = YiDianZiXun.appKey.rawValue
        let string = appKey.md5 + nonce + "\(timestamp)"
        return string.sha1
//        return (appKey.md5 + nonce + "\(timestamp)").sha1
    }
}


extension MLocationApi: TargetType {

    /// The target's base `URL`.
    var baseURL: URL {
        #if DEBUG
        let base = "http://o.go2yd.com"
        #else
        let base = "http://o.go2yd.com"
        #endif
        return URL(string: base)!
    }

    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .list:
            return "/open-api/meiritoutiao/local"
        }
    }

    /// The HTTP method used in the request.
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }

    /// Provides stub data for use in testing.
    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }

    /// The type of HTTP task to be performed.
    var task: Task {
        switch self {
        case .list(let offset, let count, let coordinate):
            let timestamp = Int(Date().timeIntervalSince1970)
            let nonce = UUID().uuidString
            let parameters: [String: Any] = ["appid": YiDianZiXun.appID.rawValue,
                                         "secretkey": secretkey(timestamp: timestamp, nonce: nonce),
                                         "timestamp": timestamp,
                                             "nonce": nonce,
                                            "offset": "\(offset)",
                                             "count": "\(count)",
                                          "latitude": "\(coordinate.latitude)",
                                         "longitude": "\(coordinate.longitude)"]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }

    /// The headers to be used in the request.
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

