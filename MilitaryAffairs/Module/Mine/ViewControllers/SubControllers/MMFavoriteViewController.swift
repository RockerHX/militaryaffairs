//
//  MMFavoriteViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/18.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class MMFavoriteViewController: UIViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var segmentControl: UISegmentedControl?
    @IBOutlet var newsListContainer: UIView?
    @IBOutlet var videoListCntainer: UIView?

    // MARK: - Public Property -
    // MARK: - Private Property -
    private let newsesEmptyView = MEmptyView()
    private let videosEmptyView = MEmptyView()
    private var newsListController: MPNewsListViewController?
    private var videoListController: MVideoListViewController?
    private var newses = [MNews]() {
        didSet {
            newsListController?.newses = newses
        }
    }
    private var videoes = [MVideo]() {
        didSet {
            videoListController?.videos = videoes
        }
    }

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchList()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if String(describing: MPNewsListViewController.self) == identifier {
                newsListController = segue.destination as? MPNewsListViewController
                newsListController?.canDelete = true
            } else if String(describing: MVideoListViewController.self) == identifier {
                videoListController = segue.destination as? MVideoListViewController
                videoListController?.canDelete = true
            }
        }
    }

    // MARK: - Event Methods -
    @IBAction func backButtonTaped() {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func segmentControlChanged(sender: UISegmentedControl) {
        if let segment = Segment(rawValue: sender.selectedSegmentIndex) {
            switch segment {
            case .news:
                if let newsesContainer = newsListContainer,
                    let videosContainer = videoListCntainer {
                    UIView.transition(from: videosContainer, to: newsesContainer, duration: 1.0, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
                }
            case .video:
                if let newsesContainer = newsListContainer,
                    let videosContainer = videoListCntainer {
                    UIView.transition(from: newsesContainer, to: videosContainer, duration: 1.0, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
                }
            }
        }
    }

    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
    }

    private func fetchList() {
        fetchFavoriteNewses()
        fetchFavoriteVideoes()
    }
    
}


extension MMFavoriteViewController {

    enum Segment: Int {
        case news  = 0
        case video = 1
    }

}


// MARK: - Private Methods -
extension MMFavoriteViewController {

    private func fetchFavoriteNewses() {
        if let result = MDNews.fetchFavorited() {
            newses = fetchFlagObjects(caches: result, flag: .favorite)
            if newses.isEmpty {
                newsesEmptyView.show(on: newsListController!, retry: nil)
            }
        }
    }

    private func fetchFavoriteVideoes() {
        if let result = MDVideo.fetchFavorited() {
            videoes = fetchFlagObjects(caches: result, flag: .favorite)
            if videoes.isEmpty {
                videosEmptyView.show(on: videoListController!, retry: nil)
            }
        }
    }

}


extension MMFavoriteViewController: MMineSubModuleCommon {}

