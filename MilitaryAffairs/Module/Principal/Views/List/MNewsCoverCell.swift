//
//  MNewsCoverCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Kingfisher


class MNewsCoverCell: MNewsCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var coverView: UIImageView?
    
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coverView?.kf.indicatorType = .activity
    }

    // MARK: - Public Methods -
    override func display(withNews news: MNews) {
        super.display(withNews: news)
        if let cover = news.covers?.first, let url = cover.url {
            coverView?.kf.setImage(with: URL(string: url))
        }
    }

    // MARK: - Private Methods -

}

