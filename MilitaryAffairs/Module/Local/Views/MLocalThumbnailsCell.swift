//
//  MLocalThumbnailsCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Kingfisher


class MLocalThumbnailsCell: MLocalCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var leftThumbnailView: UIImageView?
    @IBOutlet var centerThumbnailView: UIImageView?
    @IBOutlet var rightThumbnailView: UIImageView?
    
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftThumbnailView?.kf.indicatorType = .activity
        centerThumbnailView?.kf.indicatorType = .activity
        rightThumbnailView?.kf.indicatorType = .activity
    }

    // MARK: - Public Methods -
    override func display(with local: MLocal) {
        super.display(with: local)
        if let url = local.thumbnails?.first {
            leftThumbnailView?.kf.setImage(with: URL(string: url))
        }
        if let url = local.thumbnails?[1] {
            centerThumbnailView?.kf.setImage(with: URL(string: url))
        }
        if let url = local.thumbnails?[2] {
            rightThumbnailView?.kf.setImage(with: URL(string: url))
        }
    }

    // MARK: - Private Methods -

}

