//
//  MLocalCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Devices


class MLocalCell: UITableViewCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var provenanceLabel: UILabel?
    @IBOutlet var timeLabel: UILabel?
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Public Methods -
    public func display(with local: MLocal) {
        titleLabel?.text = local.title
        provenanceLabel?.text = MAppConfigurationManager.manager.auditing ? Device.Application.shared.name : local.source
        timeLabel?.text = local.localeTime
    }
    
    // MARK: - Private Methods -

}

