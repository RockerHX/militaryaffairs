//
//  MNewsVideoDetailViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/17.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import ZFPlayer
import SnapKit
import Devices


class MNewsVideoDetailViewController: UIViewController {

    // MARK: - IBOutlet Property -
    @IBOutlet var topPlaceholderConstraint: NSLayoutConstraint?
    @IBOutlet var playerContainer: UIImageView?
    @IBOutlet var promptLabel: UILabel?
    @IBOutlet var favoriteButton: UIButton?

    // MARK: - Public Property -
    public var news: MNews?

    // MARK: - Private Property -
    private let kVideoCover = "https://upload-images.jianshu.io/upload_images/635942-14593722fe3f0695.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240"
    private var recommendViewController: MNewsVideoRecommendViewController?
    private var player: ZFPlayerController?
    private let controlView = ZFPlayerControlView()

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        if let coverURL = URL(string: kVideoCover) {
            playerContainer?.showImage(with: coverURL, placeholder: UIColor.lightGray.toImage())
        }
        if !Device.isFringe {
            topPlaceholderConstraint?.constant = 20
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player?.currentPlayerManager.pause()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if String(describing: MNewsVideoRecommendViewController.self) == identifier {
                recommendViewController = segue.destination as? MNewsVideoRecommendViewController
                recommendViewController?.id = news?.id
            }
        }
    }

    // MARK: - Event Methods -
    @IBAction func favoriteButtonTaped(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        news?.favorited(sender.isSelected)
    }

    @IBAction func shareButtonTaped() {
        if let title = news?.title,
           let url = news?.url {
            MShareService.share(withTitle: title, prompt: (news?.summary ?? title), url: url)
        }
    }
    
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        configurePlayer()
        displayFavoriteState()
    }

    private func configurePlayer() {
        if let videoURL = news?.videoURL,
           let url = URL(string: videoURL),
           let container = playerContainer {
            let manager = ZFAVPlayerManager()
            manager.shouldAutoPlay = true
            controlView.fastViewAnimated = true
            controlView.autoHiddenTimeInterval = 5
            controlView.autoFadeTimeInterval = 0.5
            controlView.prepareShowLoading = true
            controlView.prepareShowControlView = false
            player = ZFPlayerController.init(playerManager: manager, containerView: container)
            player?.controlView = controlView
            player?.pauseWhenAppResignActive = true
            player?.assetURLs = [url]
            player?.orientationWillChange = { (_, isFullScreen) in
                (UIApplication.shared.delegate as! AppDelegate).allowOrentitaionRotation = isFullScreen
            }
            player?.playerDidToEnd = { _ in
                self.player?.stop()
            }

            player?.playTheIndex(0)
        }
    }

    private func displayFavoriteState() {
        if let favorite = news?.favorite() {
            favoriteButton?.isSelected = favorite
        }
    }
    
}


// MARK: - BoardInstance -
extension MNewsVideoDetailViewController: BoardInstance {

    enum Board: String, BoardType {
        case Video
    }

    static func instance() -> MNewsVideoDetailViewController {
        return EasyBoard<Board, MNewsVideoDetailViewController>.viewController(storyBoard: .Video)
    }
}


extension MNewsVideoDetailViewController {

    override var prefersStatusBarHidden: Bool {
        return false
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .none
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

}

