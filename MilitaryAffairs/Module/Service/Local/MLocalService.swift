//
//  MLocalService.swift
//  MilitaryAffairs
//
//  Created by RockerHX on 2018/6/22.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import RxSwift
import SwiftyJSON
import PromiseKit
import CoreLocation


struct MLocalService {

    private let disposeBag = DisposeBag()

    private func fetchServerLocals(with offset: Int, count: Int, coordinate: CLLocationCoordinate2D) -> Promise<[MLocal]> {
        return Promise { result in
            let provider = MoyaProvider<MLocationApi>()
            provider.rx.request(.list(offset: offset, count: count, coordinate: coordinate))
                .filterSuccessfulStatusCodes()
                .mapLocals()
                .subscribe({ (event) in
                    switch event {
                    case .next(let locals):
                        result.fulfill(locals)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension MLocalService {

    public func fetchLocals(offset: Int, count: Int, with coordinate: CLLocationCoordinate2D) -> Promise<[MLocal]> {
        return fetchServerLocals(with: offset, count: count, coordinate: coordinate)
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    fileprivate func mapLocals() -> Observable<[MLocal]> {
        return flatMap { response -> Single<[MLocal]> in
            return Single.just(try response.mapLocals())
        }.asObservable()
    }

}


extension Response {

    fileprivate func mapLocals() throws -> [MLocal] {
        do {
            var locals = [MLocal]()
            let json = try JSON(data: data)
            let code = json["code"].intValue
            let tip = json["status"].stringValue
            if 0 == code {
                let jsons = json["result"].arrayValue
                for json in jsons {
                    let local = try JSONDecoder().decode(MLocal.self, from: json.rawData())
                    local.hanleModel()
                    locals.append(local)
                }
            } else {
                throw MoyaError.requestMapping(tip)
            }
            return locals
        } catch {
            throw error
        }
    }

}

