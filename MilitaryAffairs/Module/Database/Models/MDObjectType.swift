//
//  MDObjectType.swift
//  News
//
//  Created by RockerHX on 2018/3/20.
//  Copyright © 2018年 RockerHX. All rights reserved.
//

import RealmSwift


protocol MDObjectType {

    typealias CodeClosure = () -> Void

    associatedtype Element where Element: Object

    var data: Data? {get}
    var readDate: Date? {get}
    var favoriteDate: Date? {get}

    static func fetchAll() -> [Element]?
    static func fetch(withID id: String) -> [Element]?
    static func fetchReaded() -> [Element]?
    static func fetchFavorited() -> [Element]?
    static func filter(_ predicate: String?) -> [Element]?

}


extension MDObjectType {
    
    static func fetchAll() -> [Element]? {
        return filter(nil)
    }

    static func fetch(withID id: String) -> [Element]? {
        return filter("id == '\(id)'")
    }

    static func fetchReaded() -> [Element]? {
        return filter("read == YES")
    }

    static func fetchFavorited() -> [Element]? {
        return filter("favorite == YES")
    }
    
}

