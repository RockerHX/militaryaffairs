//
//  MVideoListViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit
import PromiseKit
import CRRefresh
import RxSwift


class MVideoListViewController: UITableViewController {

    typealias ListType = ModuleFlag
    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var category: String?
    public var canDelete = false
    public var type: ListType = .favorite
    public var videos = [Any]() {
        didSet {
            if shouldReload {
                if videos.count != oldValue.count {
                    hanleEmpty()
                }
                refreshFinished()
            } else {
                hanleEmpty()
            }
        }
    }

    // MARK: - Private Property -
    private let emptyView = MEmptyView()
    private let noNetworkView = MNoNetworkView()
    private var viewModel = MVideoViewModel()
    private var shouldReload = true

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshList()
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if MVideoSegue.List2Detial.identifier == identifier {
                let detailViewController = segue.destination as? MVideoDetailViewController
                detailViewController?.video = sender as? MVideo
            } else if let typedInfo = R.segue.mVideoListViewController.list2Web(segue: segue) {
                let video = sender as? MVideo
                let webViewController = typedInfo.destination
                webViewController.title = video?.title
                webViewController.url = video?.h5URL
            }
        }
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
        viewModel.category = category
        setupHeaderRefreshControl()
        setupBindings()
    }

    private func setupHeaderRefreshControl() {
        if category != nil {
            tableView.cr.addHeadRefresh(animator: FastAnimator()) { [weak self] in
                self?.refresh()
            }
        }
    }

    private func setupFooterRefreshControl() {
        tableView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
            self?.loadMore()
        }
    }

    private func setupRefreshControl() {
        if nil == tableView.cr.header {
            setupHeaderRefreshControl()
        }
        if nil == tableView.cr.footer {
            setupFooterRefreshControl()
        }
    }

    private func setupBindings() {
        viewModel.list.observeOn(MainScheduler.instance)
        .subscribe(onNext: { [weak self] (result) in
            switch result {
            case .success(let list):
                self?.videos = list
            case .failure(let error):
                switch error {
                case .underlying(_, _):
                    self?.hanleNetwork()
                default:
                    self?.hanleEmpty()
                }
                self?.refreshFinished()
                self?.showError(error: error)
            }
        }, onError: { [weak self] (error) in
            self?.hanleNetwork()
            self?.refreshFinished()
            self?.showError(error: error)
        }).disposed(by: viewModel.disposeBag)
    }

    private func refreshList() {
        if videos.isEmpty && (category != nil) {
            tableView.cr.beginHeaderRefresh()
        }
    }

    private func refresh() {
        viewModel.reloadAction.asObserver().onNext(())
    }

    private func loadMore() {
        viewModel.loadMoreAction.asObserver().onNext(())
    }

    private func refreshFinished() {
        self.tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.tableView.cr.endHeaderRefresh()
            self.tableView.cr.endLoadingMore()
        })
    }

    private func hanleEmpty() {
        if videos.isEmpty {
            tableView.cr.removeHeader()
            tableView.cr.removeFooter()
            emptyView.show(on: self) { [weak self] in
                self?.refresh()
            }
        } else {
            emptyView.hidden()
            noNetworkView.hidden()
            if !canDelete {
                setupRefreshControl()
            }
        }
    }

    private func hanleNetwork() {
        if videos.isEmpty {
            tableView.cr.removeHeader()
            tableView.cr.removeFooter()
            noNetworkView.show(on: self) { [weak self] in
                self?.refresh()
            }
        }
    }

}


extension MVideoListViewController: BoardInstance {

    enum List: String, BoardType {
        case Video
    }

    static func instance() -> MVideoListViewController {
        return EasyBoard<List, MVideoListViewController>.viewController(storyBoard: .Video)
    }
}


// MARK: - Table View Data Source Methods -
extension MVideoListViewController {

    // MARK: - Table View Data Source Methods -
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = videos[indexPath.row]
        if let video = object as? MVideo {
            let cell = tableView.dequeueReusableCell(withIdentifier: MVideoCell.identifier(), for: indexPath) as! MVideoCell
            cell.dispay(withVideo: video)
            return cell
        } else if let adView = object as? BUNativeExpressAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: MAdvertCell.identifier(), for: indexPath) as! MAdvertCell
            cell.showAD(adView: adView)
            return cell
        }
        return UITableViewCell()
    }

}


// MARK: - Table View Delegate Methods -
extension MVideoListViewController {

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let object = videos[indexPath.row]
        if let adView = object as? BUNativeExpressAdView {
            return adView.frame.size.height
        }
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = videos[indexPath.row]
        guard let video = object as? MVideo,
              let mode = video.mode
        else { return }
        switch mode {
        case .native:
            video.readed()
            MVideoSegue.List2Detial.performSegue(on: self, sender: video)
        case .store:
            if let source = video.h5URL, let url = URL(string: source) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case .web:
            performSegue(withIdentifier: R.segue.mVideoListViewController.list2Web, sender: video)
        }
    }

}


extension MVideoListViewController {

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return canDelete
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if .delete == editingStyle {
            if let video = videos[indexPath.row] as? MVideo {
                switch type {
                case .favorite:
                    video.unFavorite()
                case .read:
                    video.unRead()
                }
            }
            shouldReload = false
            videos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }

}

