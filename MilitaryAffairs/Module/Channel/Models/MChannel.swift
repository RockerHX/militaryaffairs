//
//  MChannel.swift
//  News
//
//  Created by RockerHX on 2018/3/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift


class MChannel: Codable {

    typealias State = MAppConfiguration.State

    enum `Type`: String, Codable {
        case news  = "NEWS"
        case video = "VIDEO"
        case dress = "DRESS"
        case military = "MilitaryAffairs"
    }

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case state = "status"
    }

    var id: Int? = nil
    var name: String? = nil
    var type: Type = .military
    var state: State? = nil
}


//extension Encodable {
//    var dictionary: [String: Any]? {
//        guard let data = try? JSONEncoder().encode(self) else { return nil }
//        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
//    }
//}


extension MChannel {

    var dictionary: [String: String]? {
        if let cid = id, let cname = name, let cstate = state {
            return [
                 "id": "\(cid)",
               "name": cname,
               "type": type.rawValue,
             "status": cstate.rawValue,
            ]
        }
        return nil
    }

}


extension MChannel {

    func cache(isVideo: Bool) {
        if let cid = id, let cstatus = state?.rawValue {
            do {
                let realm = try Realm()
                if let dbChannel = realm.objects(MDChannel.self).filter("id = '\(cid)'").first {
                    _ = dbChannel.update {
                        dbChannel.name = name
                        dbChannel.status = cstatus
                    }
                } else {
                    if var object = self.dictionary {
                        if isVideo {
                            object["type"] = Type.video.rawValue
                        }
                        let channel = MDChannel(value: object)
                        _ = channel.save {}
                    }
                }
            } catch {
                debugPrint(error)
            }
        }
    }

    static func cache(object: [String: String]) {
        if let id = object["id"], let name = object["name"], let status = object["status"] {
            do {
                let realm = try Realm()
                if let dbChannel = realm.objects(MDChannel.self).filter("id = '\(id)'").first {
                    _ = dbChannel.update {
                        dbChannel.name = name
                        dbChannel.status = status
                    }
                } else {
                    let channel = MDChannel(value:object)
                    _ = channel.save {}
                }
            } catch {
                debugPrint(error)
            }
        }
    }

}


