//
//  MDetailAdvertPool.swift
//  News
//
//  Created by RockerHX on 2018/3/20.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Foundation


class MDetailAdvertPool: NSObject {

    typealias ReloadClosure = (MDetailAdvertPool) -> Void

    // MARK: Singleton
    static let shared = MDetailAdvertPool()

    // MARK: Public Property
    public var largeADAdverts = [BUNativeExpressAdView]()
    public var largeADAdvertsWeight = [BUNativeExpressAdView: Int]()
    public var imageTextADAdverts = [BUNativeExpressAdView]()
    public var imageTextADAdvertsWeight = [BUNativeExpressAdView: Int]()

    // MARK: Private Property
    private let largeADInterval = 1
    private let largeADLoadCount = 2
    private let largeADThreshold = 8
    private let imageTextADInterval = 3
    private let imageTextADLoadCount = 5
    private let imageTextADThreshold = 20
    private var largeADManager: BUNativeExpressAdManager?
    private var imageTextADManager: BUNativeExpressAdManager?
    private var reloadClosure: ReloadClosure?

    // MARK: Initialize Methods
    fileprivate override init() {
        super.init()
        let screenWidth = ((UIApplication.shared.delegate?.window)!)!.bounds.size.width
        let largeSize = CGSize(width: screenWidth, height: screenWidth/1.76)
        let imageTextSize = CGSize(width: screenWidth, height: 120)

        let configuration = MAppConfigurationManager.manager.configuration
        if let platform = configuration?.platforms?.first {
            platform.adverts?.forEach({ (advert) in
                if let kind = advert.kind, kind == .native {
                    advert.subs?.forEach({ (nativeAdvert) in
                        if let style = nativeAdvert.style,
                            let posid = nativeAdvert.posid {
                            if style == .image1280_720 {
                                setupLargeADManager(with: posid, size: largeSize)
                            } else if style == .leftImageRigthText {
                                setupImageTextADManager(with: posid, size: imageTextSize)
                            }
                        }
                    })
                }
            })
        }
    }

}


// MARK: Private Methods
extension MDetailAdvertPool {

    private func setupLargeADManager(with slotID: String, size: CGSize) {
        let slot = BUAdSlot()
        slot.id = slotID
        slot.adType = .feed
        slot.imgSize = BUSize(by: .banner600_300)
        slot.position = .feed
        largeADManager = BUNativeExpressAdManager(slot: slot, adSize: size)
        largeADManager?.delegate = self
        largeADManager?.loadAdData(withCount: largeADLoadCount)
    }

    private func setupImageTextADManager(with slotID: String, size: CGSize) {
        let slot = BUAdSlot()
        slot.id = slotID
        slot.adType = .feed
        slot.imgSize = BUSize(by: .banner600_150)
        slot.position = .feed
        imageTextADManager = BUNativeExpressAdManager(slot: slot, adSize: size)
        imageTextADManager?.delegate = self
        imageTextADManager?.loadAdData(withCount: imageTextADLoadCount)
    }

    private func takeoutLargeAdverts(count: Int) -> [BUNativeExpressAdView] {
        var weights = [BUNativeExpressAdView: Int]()
        largeADAdverts.forEach { [unowned self] (advert) in
            weights[advert] = self.largeADAdvertsWeight[advert]
        }
        let sortedAdverts = weights.sorted { $0.value < $1.value }
        let lowExposureRateAdverts = sortedAdverts.map { $0.key }
        let intervalAdverts = lowExposureRateAdverts.prefix(count)
        return Array(intervalAdverts)
    }

    private func takeoutImageTextAdverts(count: Int) -> [BUNativeExpressAdView] {
        var weights = [BUNativeExpressAdView: Int]()
        imageTextADAdverts.forEach { [unowned self] (advert) in
            weights[advert] = self.imageTextADAdvertsWeight[advert]
        }
        let sortedAdverts = weights.sorted { $0.value < $1.value }
        let lowExposureRateAdverts = sortedAdverts.map { $0.key }
        let intervalAdverts = lowExposureRateAdverts.prefix(count)
        return Array(intervalAdverts)
    }

}


// MARK: Public Methods
extension MDetailAdvertPool {

    public func reload(success: @escaping ReloadClosure) {
        reloadClosure = success
        if (largeADAdverts.count >= largeADThreshold) ||
            (imageTextADAdverts.count >= imageTextADThreshold) {
            success(self)
        } else {
            if (nil == largeADManager) && (nil == imageTextADManager) {
                success(self)
            } else {
                largeADManager?.loadAdData(withCount: largeADLoadCount)
                imageTextADManager?.loadAdData(withCount: imageTextADLoadCount)
            }
        }
    }

    public func packageList(list: [Any]) -> [Any] {
        if (nil == largeADManager) && (nil == imageTextADManager) {
            return list
        }
        var source = Array(list)
        var collection = [Any]()
        let adCount = source.count / imageTextADInterval
        let ads = takeoutImageTextAdverts(count: adCount)
        repeat {
            ads.forEach { (advert) in
                if source.count > imageTextADInterval {
                    (0..<imageTextADInterval).forEach({ _ in
                        collection.append(source.removeFirst())
                    })
                    collection.append(advert)
                }
            }
        } while (source.count > imageTextADInterval) && (ads.count > 0)

        collection.append(contentsOf: source)
        let largeAdvert = takeoutLargeAdverts(count: 1)
        if !largeAdvert.isEmpty {
            collection.insert(largeAdvert.first!, at: 0)
        }
        return collection
    }

}


// MARK: BUNativeExpressAdViewDelegate Methods
extension MDetailAdvertPool: BUNativeExpressAdViewDelegate {

    func nativeExpressAdSuccess(toLoad nativeExpressAd: BUNativeExpressAdManager, views: [BUNativeExpressAdView]) {
        if nativeExpressAd == largeADManager {
            let last = views.count
            let first = (last < largeADLoadCount) ? 0 : (last - largeADLoadCount)
            let newAds = Array(views[first..<last])
            largeADAdverts.append(contentsOf: newAds)
            let entry = ((UIApplication.shared.delegate?.window)!)!.rootViewController as! MEntryViewController
            let mainViewController = entry.mainViewController!
            newAds.forEach { [unowned self] (advert) in
                self.largeADAdvertsWeight[advert] = 0
                advert.removeFromSuperview()
                advert.rootViewController = mainViewController
                advert.render()
            }
        } else if nativeExpressAd == imageTextADManager {
            let last = views.count
            let first = (last < imageTextADLoadCount) ? 0 : (last - imageTextADLoadCount)
            let newAds = Array(views[first..<last])
            imageTextADAdverts.append(contentsOf: newAds)
            let entry = ((UIApplication.shared.delegate?.window)!)!.rootViewController as! MEntryViewController
            let mainViewController = entry.mainViewController!
            newAds.forEach { [unowned self] (advert) in
                self.imageTextADAdvertsWeight[advert] = 0
                advert.removeFromSuperview()
                advert.rootViewController = mainViewController
                advert.render()
            }
        }

        if !largeADAdverts.isEmpty && !imageTextADAdverts.isEmpty {
            reloadClosure?(self)
        }
    }

    func nativeExpressAdFail(toLoad nativeExpressAd: BUNativeExpressAdManager, error: Error?) {
        print("nativeExpressAdFail: \(String(describing: error))")
        reloadClosure?(self)
    }

    func nativeExpressAdViewWillShow(_ nativeExpressAdView: BUNativeExpressAdView) {
        if let weight = imageTextADAdvertsWeight[nativeExpressAdView] {
            imageTextADAdvertsWeight[nativeExpressAdView] = (weight + 1)
        }
        if let weight = largeADAdvertsWeight[nativeExpressAdView] {
            largeADAdvertsWeight[nativeExpressAdView] = (weight + 1)
        }
    }

}


