//
//  AppDelegate.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


enum UMeng: String {
    case appKey = "5b3a11d9f43e4802bb000261"
    #if DEBUG
    case channel = "Test Version"
    #else
    case channel = "App Store"
    #endif

    enum Share {
        enum Wechat: String {
            case AppID  = "wxacd4324c632b9b03"
            case AppKey = "9051812a63c87b09902d22e70c8f05e2"
        }
        enum QQ: String {
            case AppID  = "1108022200"
            case AppKey = "J401YosD3POVGeYv"
        }
    }
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var messageNotification: MMessageNotification?
    var allowOrentitaionRotation = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // 友盟SDK初始化
        UMConfigure.initWithAppkey(UMeng.appKey.rawValue, channel: UMeng.channel.rawValue)
        // Push组件基本功能配置
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        #if DEBUG
            UMessage.openDebugMode(true)
        #else
            UMessage.openDebugMode(false)
        #endif
        let entity = UMessageRegisterEntity()
        UMessage.registerForRemoteNotifications(launchOptions: launchOptions, entity: entity) { (granted, error) in
            if granted {
                // 用户选择了接收Push消息
                debugPrint("用户选择了接收Push消息")
            } else {
                // 用户拒绝接收Push消息
                debugPrint("用户拒绝接收Push消息")
            }
        }
        // 分享组件基本功能配置
        configureUSharePlatforms()

        _ = LaunchManager.manager
        return true
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UMessage.setAutoAlert(false)
        if #available(iOS 10.0, *) {
            UMessage.didReceiveRemoteNotification(userInfo)
            completionHandler(.newData)
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return UMSocialManager.default().handleOpen(url)
    }

}


extension AppDelegate: UNUserNotificationCenterDelegate {

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        debugPrint(token)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint(error)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        UMessage.setAutoAlert(false)
        if #available(iOS 10.0, *) {
            UMessage.didReceiveRemoteNotification(userInfo)
        }
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // 应用处于前台时的远程推送接受
//        receiveNotification(center, willPresent: notification)
        completionHandler([.alert, .sound, .badge])
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // 应用处于后台时的远程推送接受
        receiveNotification(center, willPresent: response.notification)
    }

    @available(iOS 10.0, *)
    private func receiveNotification(_ center: UNUserNotificationCenter, willPresent notification: UNNotification) {
        let userInfo = notification.request.content.userInfo
        do {
            let data = try JSONSerialization.data(withJSONObject: userInfo, options: [])
            let message = try data.decoded() as MMessageNotification
            message.parseObject()
            let mainViewController = (window?.rootViewController as? MEntryViewController)?.mainViewController
            if nil == mainViewController {
                messageNotification = message
            } else {
                mainViewController?.recevieMessageNotification(message)
            }
        } catch {
            debugPrint(error)
        }
        if let trigger = notification.request.trigger {
            if trigger.isKind(of: UNPushNotificationTrigger.self) {
                // 关闭U-Push自带的弹出框
                UMessage.setAutoAlert(false)
                UMessage.didReceiveRemoteNotification(userInfo)
            } else {
                // 应用处于前台时的本地推送接受
            }
        }
    }

}


extension AppDelegate {

    private func configureUSharePlatforms() {
        UMSocialManager.default().setPlaform(.wechatSession, appKey: UMeng.Share.Wechat.AppID.rawValue, appSecret: UMeng.Share.Wechat.AppKey.rawValue, redirectURL: "http://mobile.umeng.com/social")
        UMSocialManager.default().setPlaform(.wechatTimeLine, appKey: UMeng.Share.Wechat.AppID.rawValue, appSecret: UMeng.Share.Wechat.AppKey.rawValue, redirectURL: "http://mobile.umeng.com/social")
        UMSocialManager.default().setPlaform(.wechatFavorite, appKey: UMeng.Share.Wechat.AppID.rawValue, appSecret: UMeng.Share.Wechat.AppKey.rawValue, redirectURL: "http://mobile.umeng.com/social")
        UMSocialManager.default().setPlaform(.QQ, appKey: UMeng.Share.QQ.AppID.rawValue, appSecret: "", redirectURL: "http://mobile.umeng.com/social")
        UMSocialManager.default().setPlaform(.qzone, appKey: UMeng.Share.QQ.AppID.rawValue, appSecret: "", redirectURL: "http://mobile.umeng.com/social")
    }

}


extension AppDelegate {

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if allowOrentitaionRotation {
            return .allButUpsideDown
        }
        return .portrait
    }

}

