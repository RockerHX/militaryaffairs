//
//  MNewsThumbnailsCell.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Kingfisher


class MNewsThumbnailsCell: MNewsCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var leftThumbnailView: UIImageView?
    @IBOutlet var centerThumbnailView: UIImageView?
    @IBOutlet var rightThumbnailView: UIImageView?
    
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftThumbnailView?.kf.indicatorType = .activity
        centerThumbnailView?.kf.indicatorType = .activity
        rightThumbnailView?.kf.indicatorType = .activity
    }

    // MARK: - Public Methods -
    override func display(withNews news: MNews) {
        super.display(withNews: news)
        if let thumbnail = news.thumbnails?.first, let url = thumbnail.url {
            leftThumbnailView?.kf.setImage(with: URL(string: url))
        }
        if let thumbnail = news.thumbnails?[1], let url = thumbnail.url {
            centerThumbnailView?.kf.setImage(with: URL(string: url))
        }
        if let thumbnail = news.thumbnails?[2], let url = thumbnail.url {
            rightThumbnailView?.kf.setImage(with: URL(string: url))
        }
    }

    // MARK: - Private Methods -

}

