//
//  MDNews.swift
//  News
//
//  Created by RockerHX on 2018/3/7.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift
import DateToolsSwift


extension MDNews: MDObjectType {
    typealias Element = MDNews
}


class MDNews: Object {

    @objc dynamic var id: String? = nil
    @objc dynamic var cid: String? = nil
    @objc dynamic var data: Data? = nil
    @objc dynamic var read = false
    @objc dynamic var readDate: Date? = nil
    @objc dynamic var favorite = false
    @objc dynamic var favoriteDate: Date? = nil

    @objc dynamic var createdDate = Date()
    @objc dynamic var updatedDate: Date? = nil
    @objc dynamic var expiredDate = 30.minutes.later

    override static func indexedProperties() -> [String] {
        return ["id", "favoriteDate", "createdDate", "updatedDate", "expiredDate"]
    }

    override static func primaryKey() -> String? {
        return "id"
    }

}


// MARK: - Static Methods -
extension MDNews {

    static func fetch(withChannelID cid: String) -> [MDNews]? {
        return filter("cid == '\(cid)'")
    }

    static func filter(_ predicate: String?) -> [MDNews]? {
        var newses = [MDNews]()
        do {
            let realm = try Realm()
            var results: Results<MDNews>
            if let filterCondition = predicate {
                results = realm.objects(MDNews.self).filter(filterCondition)
            } else {
                results = realm.objects(MDNews.self)
            }
            let date = Date()
            var expiredNewses = [MDNews]()
            results.forEach({ (news) in
                if news.expiredDate < date {
                    expiredNewses.append(news)
                } else {
                    if (news.cid != nil) && (news.data != nil) {
                        newses.append(news)
                    }
                }
            })
            deleteExpired(newses: expiredNewses)
            return newses
        } catch {
            debugPrint(error)
            return nil
        }
    }

    private static func deleteExpired(newses: [MDNews]) {
        newses.forEach { (object) in
            let newsRef = ThreadSafeReference(to: object)
            DispatchQueue(label: "background").async {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        guard let news = realm.resolve(newsRef) else {
                            return // news 被删除
                        }
                        try realm.write {
                            realm.delete(news)
                        }
                    } catch {
                        debugPrint("deleteExpired Error: \(error)")
                    }
                }
            }
        }
    }

}


// MARK: - Public Methods -
extension MDNews {

    public func save(update: Bool = false, code: CodeClosure) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                updatedDate = Date()
                code()
                if !update {
                    realm.add(self, update: .all)
                }
            }
        } catch {
            debugPrint(error)
            return false
        }
        return true
    }

    public func readed() -> Bool {
        return save(update: true, code: {
            read = true
            readDate = Date()
        })
    }

    public func unRead() -> Bool {
        return save(update: true, code: {
            read = false
            readDate = nil
        })
    }

    public func favorited(_ favorited: Bool) -> Bool {
        return save(update: true, code: {
            favorite = favorited
            if favorited {
                favoriteDate = Date()
            }
        })
    }

    public func unFavorite() -> Bool {
        return save(update: true, code: {
            favorite = false
            favoriteDate = nil
        })
    }

    public func update(code: CodeClosure) -> Bool {
        return save(update: true, code: code)
    }

}

