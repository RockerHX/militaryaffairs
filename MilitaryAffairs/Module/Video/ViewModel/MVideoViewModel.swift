//
//  MVideoViewModel.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RxSwift
import Moya


class MVideoViewModel {

    // MARK: - Public Property -
    public var category: String?
    public let disposeBag = DisposeBag()

    // MARK: - Private Property -
    private let service = MVideoService()
    private var listCollection = [Any]()

    // MARK: - Inputs -
    let reloadAction = PublishSubject<Void>()
    let loadMoreAction = PublishSubject<Void>()

    // MARK: - Outputs -
    public private(set) var list = PublishSubject<Result<[Any], MoyaError>>()

    // MARK: - Initialize Methods -
    init() {
        setupBinding()
    }

    private func setupBinding() {
        reloadAction
            .subscribe { [weak self] _ in
                self?.reload()
            }.disposed(by: disposeBag)

        loadMoreAction
            .subscribe { [weak self] _ in
                self?.loadMore()
            }.disposed(by: disposeBag)
    }

}


extension MVideoViewModel {

    private func reload() {
        listCollection.removeAll()
        fetchVideos()
    }

    private func loadMore() {
        fetchMoreVideos()
    }

    private func fetchVideos() {
        if let videoCategory = category {
            service.fetchVideos(withCategory: videoCategory)
                .done { [weak self] videoList in
                    self?.updateVideoCollection(videoList)
                }.catch { [weak self] (error) in
                    if let e = error as? MoyaError {
                        self?.list.onNext(.failure(e))
                    }
                }
        }
    }

    private func fetchMoreVideos() {
        if let videoCategory = category, let date = (listCollection.last as? MVideo)?.date {
            service.fetchMoreVideos(withCategory: videoCategory, date: date)
                .done { [weak self] videoList in
                    self?.appendVideoCollection(videoList)
                }.catch { [weak self] (error) in
                    if let e = error as? MoyaError {
                        self?.list.onNext(.failure(e))
                    }
            }
        }
    }

    private func updateVideoCollection(_ collection: [Any]) {
        MAdvertPool.shared.reload { (pool) in
            let list = pool.packageList(list: collection)
            self.listCollection = collection
            self.list.onNext(.success(list))
        }
    }

    private func appendVideoCollection(_ collection: [Any]) {
        let packageList = MAdvertPool.shared.packageList(list: collection)
        listCollection.append(contentsOf: packageList)
        list.onNext(.success(listCollection))
    }

}

