//
//  MDVideo.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import RealmSwift
import DateToolsSwift


extension MDVideo: MDObjectType {
    typealias Element = MDVideo
}


class MDVideo: Object {

    typealias CodeClosure = () -> Void

    @objc dynamic var id: String? = nil
    @objc dynamic var category: String? = nil
    @objc dynamic var data: Data? = nil
    @objc dynamic var read = false
    @objc dynamic var readDate: Date? = nil
    @objc dynamic var favorite = false
    @objc dynamic var favoriteDate: Date? = nil

    @objc dynamic var createdDate = Date()
    @objc dynamic var updatedDate: Date? = nil
    @objc dynamic var expiredDate = 30.minutes.later

    override static func indexedProperties() -> [String] {
        return ["id", "favoriteDate", "createdDate", "updatedDate", "expiredDate"]
    }

    override static func primaryKey() -> String? {
        return "id"
    }
    
}


// MARK: - Static Methods -
extension MDVideo {

    static func fetch(withCategory category: String) -> [MDVideo]? {
        return filter("category == '\(category)'")
    }

    static func filter(_ predicate: String?) -> [MDVideo]? {
        var videos = [MDVideo]()
        do {
            let realm = try Realm()
            var results: Results<MDVideo>
            if let filterCondition = predicate {
                results = realm.objects(MDVideo.self).filter(filterCondition)
            } else {
                results = realm.objects(MDVideo.self)
            }
            let date = Date()
            var expiredVideos = [MDVideo]()
            results.forEach({ (video) in
                if video.expiredDate < date {
                    expiredVideos.append(video)
                } else {
                    if (video.category != nil) && (video.data != nil) {
                        videos.append(video)
                    }
                }
            })
            deleteExpired(videos: expiredVideos)
            return videos
        } catch {
            debugPrint(error)
            return nil
        }
    }

    private static func deleteExpired(videos: [MDVideo]) {
        videos.forEach { (object) in
            let videoRef = ThreadSafeReference(to: object)
            DispatchQueue(label: "background").async {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        guard let video = realm.resolve(videoRef) else {
                            return // video 被删除
                        }
                        try realm.write {
                            realm.delete(video)
                        }
                    } catch {
                        debugPrint("deleteExpired Error: \(error)")
                    }
                }
            }
        }
    }

}


// MARK: - Public Methods -
extension MDVideo {

    public func save(update: Bool = false, code: CodeClosure) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                updatedDate = Date()
                code()
                if !update {
                    realm.add(self, update: .all)
                }
            }
        } catch {
            debugPrint(error)
            return false
        }
        return true
    }

    public func readed() -> Bool {
        return save(update: true, code: {
            read = true
            readDate = Date()
        })
    }

    public func unRead() -> Bool {
        return save(update: true, code: {
            read = false
            readDate = nil
        })
    }

    public func favorited(_ favorited: Bool) -> Bool {
        return save(update: true, code: {
            favorite = favorited
            if favorited {
                favoriteDate = Date()
            }
        })
    }

    public func unFavorite() -> Bool {
        return save(update: true, code: {
            favorite = false
            favoriteDate = nil
        })
    }

    public func update(code: CodeClosure) -> Bool {
        return save(update: true, code: code)
    }

}

