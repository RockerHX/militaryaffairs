//
//  PagesControlViewController.swift
//  News
//
//  Created by RockerHX on 2018/3/5.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit


protocol PagesControlViewControllerDelegate {
    func controlPageChanged(toIndex index: Int)
}


class PagesControlViewController: UICollectionViewController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var delegate: PagesControlViewControllerDelegate?
    public var titles = [String]() {
        didSet {
            reload()
        }
    }

    public var index = 0 {
        didSet {
            moveBottomLine(toIndex: index)
        }
    }
    
    // MARK: - Private Property -
    private var animationBottomLine: UIView?
    private var selectedIndex = 0
    private var selectedTitle = ""

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !titles.contains(selectedTitle) {
            reloadBottomLine()
        }
    }

    /*
    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        if !titles.isEmpty {
            selectedTitle = titles[selectedIndex]
        }
    }

    private func reload() {
        selectedIndex = titles.firstIndex(of: selectedTitle) ?? 0
        if 0 == selectedIndex {
            selectedTitle = ""
        } else {
            index = selectedIndex
        }
        delegate?.controlPageChanged(toIndex: selectedIndex)
        let shouldDisplay = !titles.contains(selectedTitle)
        collectionView?.performBatchUpdates({
            let section = IndexSet(integer: 0)
            collectionView?.reloadSections(section)
        }, completion: { [weak self] (finished) in
            self?.reloadDisplay(shouldDisplay)
        })
    }

    private func reloadDisplay(_ display: Bool) {
        if display {
            let left = CGRect(x: 0, y: 0, width: 1, height: 1)
            collectionView?.scrollRectToVisible(left, animated: false)
        } else {
            let selectedIndexPath = IndexPath(item: selectedIndex, section: 0)
            collectionView?.scrollToItem(at: selectedIndexPath, at: .centeredHorizontally, animated: true)
        }
    }

    private func reloadBottomLine() {
        if nil == animationBottomLine {
            animationBottomLine = generateBottomLine()
        }
        animationBottomLine?.backgroundColor = HXTheme.current().color

        let firstIndexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: firstIndexPath) as? PagesControlCell {
            cell.isSelected = true
            moveBottomLine(toCell: cell)
        }
    }

    private func generateBottomLine() -> UIView? {
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? PagesControlCell {
            let height: CGFloat = 2
            let bottomLine = UIView(frame: CGRect(x: 0,
                                                  y: view.frame.height - height,
                                                  width: cell.frame.width,
                                                  height: height))
            collectionView?.addSubview(bottomLine)
            return bottomLine
        } else {
            return nil
        }
    }

    private func moveBottomLine(toIndex index: Int) {
        collectionView?.reloadData()
        selectedIndex = index
        selectedTitle = titles[index]
        let indexPath = IndexPath(item: index, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        let pose = collectionView!.collectionViewLayout.layoutAttributesForItem(at: indexPath)!
        moveBottomLine(to: pose)
        collectionView?.reloadData()
    }

    private func moveBottomLine(to layout: UICollectionViewLayoutAttributes) {
        if var frame = animationBottomLine?.frame {
            frame.size.width = layout.frame.size.width
            var center = animationBottomLine?.center
            center!.x = layout.center.x
            _ = UIView.Animator(duration: 0.3).animations {
                self.animationBottomLine?.frame = frame
                self.animationBottomLine?.center = center!
            }
        }
    }

    private func moveBottomLine(toCell: PagesControlCell?) {
        if let cell = toCell, var frame = animationBottomLine?.frame,
            let indexPath = collectionView?.indexPath(for: cell) {
            collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            frame.size.width = cell.frame.size.width
            var center = animationBottomLine?.center
            center!.x = cell.center.x
            _ = UIView.Animator(duration: 0.3).animations {
                self.animationBottomLine?.frame = frame
                self.animationBottomLine?.center = center!
            }
        }
    }

}


// MARK: - Collection View Data Source Methods -
extension PagesControlViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PagesControlCell.identifier(), for: indexPath) as! PagesControlCell
        // Configure the cell
        cell.display(withTitle: titles[indexPath.row], selected: (indexPath.row == selectedIndex))

        return cell
    }

}


// MARK: - Collection View Delegate Methods -
extension PagesControlViewController {

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedIndexPath = IndexPath(item: selectedIndex, section: 0)
        let cell = collectionView.cellForItem(at: selectedIndexPath) as? PagesControlCell
        cell?.isSelected = false

        let index = indexPath.row
        selectedIndex = index
        selectedTitle = titles[index]
        let selectedCell = collectionView.cellForItem(at: IndexPath(item: index, section: 0)) as? PagesControlCell
        moveBottomLine(toCell: selectedCell)
        delegate?.controlPageChanged(toIndex: index)
        collectionView.reloadData()
    }

}


// MARK: - UICollectionViewDelegateFlowLayout -
extension PagesControlViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel()
        label.text = titles[indexPath.row]
        let size = label.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: size.width + 15, height: view.frame.size.height)
    }

}

