//
//  MEntryViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//  Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


class MEntryViewController: UIViewController {

    // MARK: IBOutlet Property
    // MARK: Public Property
    public var mainViewController: MMainViewController?

    // MARK: Private Property
    private var splashAdDidClicked = false

    // MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let typedInfo = R.segue.mEntryViewController.entry2Main(segue: segue) {
            mainViewController = typedInfo.destination
        }
    }

    // MARK: Event Methods
    // MARK: Public Methods
    // MARK: Private Methods
    private func configure() {
        requestIDFA {
            self.setupBUAdSDK()
            self.loadSplashAD()
        } failure: {
            self.showMainSence()
        }
    }

    private func loadSplashAD() {
        let configuration = MAppConfigurationManager.manager.configuration
        if let platform = configuration?.platforms?.first{
            platform.adverts?.forEach({ (advert) in
                if let kind = advert.kind,
                    let posid = advert.posid {
                    if kind == .splash {
                        showSplashAD(with: posid)
                    }
                }
            })
        }
    }

    private func showMainSence() {
        performSegue(withIdentifier: R.segue.mEntryViewController.entry2Main, sender: nil)
    }

    private func showSplashAD(with slotID: String) {

        func showAD(with slotID: String) {
            let frame = UIScreen.main.bounds
            let splashView = BUSplashAdView(slotID: slotID, frame: frame)
            splashView.tolerateTimeout = 10
            splashView.delegate = self
            let window = (UIApplication.shared.delegate?.window)!
            splashView.loadAdData()
            window?.rootViewController?.view.addSubview(splashView)
            splashView.rootViewController = window?.rootViewController
        }

        DispatchQueue.main.async {
            showAD(with: slotID)
        }
    }

}


import AppTrackingTransparency
import AdSupport
// MARK: BUAdSDK
extension MEntryViewController {

    fileprivate func requestIDFA(success: @escaping () -> Void, failure: @escaping () -> Void) {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                // Tracking authorization completed. Start loading ads here.
                switch status {
                case .authorized:
                    success()
                default:
                    success()
                    break
                }
            })
        } else {
            // Fallback on earlier versions
        }
    }

    fileprivate func setupBUAdSDK() {
        let configuration = MAppConfigurationManager.manager.configuration
        if let platform = configuration?.platforms?.first,
            let appID = platform.appid {
            BUAdSDKManager.setAppID(appID)
            #if DEBUG
            BUAdSDKManager.setLoglevel(.debug)
            #endif
        }
    }

}


// MARK: BUSplashAdDelegate
extension MEntryViewController: BUSplashAdDelegate {

    func splashAdDidClick(_ splashAd: BUSplashAdView) {
        splashAdDidClicked = true
    }

    func splashAdWillClose(_ splashAd: BUSplashAdView) {
        if !splashAdDidClicked {
            if let subviews =  UIApplication.shared.delegate?.window??.subviews.first?.subviews {
                subviews.forEach({ [weak self] (view) in
                    self?.view = view.snapshotView(afterScreenUpdates: true)
                })
            }
            if let rootViewController = UIApplication.shared.delegate?.window??.rootViewController,
                let presentedViewController = rootViewController.presentedViewController {
                UIView.setAnimationsEnabled(false)
                presentedViewController.modalTransitionStyle = .crossDissolve
            }
        }
    }

    func splashAdDidClose(_ splashAd: BUSplashAdView) {
        if !splashAdDidClicked {
            UIView.setAnimationsEnabled(true)
            showMainSence()
        }
    }

    func splashAdDidClickSkip(_ splashAd: BUSplashAdView) {
        UIView.setAnimationsEnabled(true)
        splashAdDidClicked = false
    }

    func splashAd(_ splashAd: BUSplashAdView, didFailWithError error: Error?) {
        debugPrint(error.debugDescription)
        showMainSence()
    }

}

