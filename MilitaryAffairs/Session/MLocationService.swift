//
//  MLocationService.swift
//  美美穿搭
//
//  Created by RockerHX on 2018/6/21.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


/*
 必须在Info.plist里添加相应权限：
 <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
 <string>是否允许定位？</string>
 或者：
 <key>NSLocationWhenInUseUsageDescription</key>
 <string>是否允许定位？</string>
 */



import CoreLocation
import PromiseKit


class MLocationService: NSObject {

    // MARK: - Singleton -
    static let service = MLocationService()

    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Initialize Methods -
    fileprivate override init() {
        super.init()
        configure()
    }

    // MARK: - Private Methods -
    private func configure() {
        _ = CLLocationManager.requestAuthorization()
    }

    fileprivate func checkServiceEnabled() -> Promise<Bool> {
        return Promise { result in
            if CLLocationManager.locationServicesEnabled() {
                result.fulfill(true)
            } else {
                let error = NSError(domain: "请先到【设置】->【隐私】->【定位服务】里打开定位！！！", code: -1, userInfo: nil)
                result.reject(error as Error)
            }
        }
    }

    fileprivate func configureService() -> Promise<Void> {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return Promise()
        default:
            _ = CLLocationManager.requestAuthorization()
            return Promise { result in
                let error = NSError(domain: "定位受到限制，请确定正确授权？", code: -1, userInfo: nil)
                result.reject(error as Error)
            }
        }
    }

}


// MARK: - Public Methods -
extension MLocationService {

    public func requestLocation() -> Promise<[CLLocation]> {
        return checkServiceEnabled()
            .then { enabled in
                self.configureService()
            }.then { _ in
                CLLocationManager.requestLocation()
            }
    }

}

