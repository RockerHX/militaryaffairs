//
//  MWeather.swift
//  美美穿搭
//
//  Created by RockerHX on 2018/6/21.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Foundation


struct MWeather: Codable {

    enum CodingKeys: String, CodingKey {
        case id                  = "cid"
        case temperature
        case date
        case week
        case bigIcon
        case cloud               = "text"
        case city

        case min                 = "low"
        case max                 = "high"
        case windDirection       = "wind_direction"
        case windDirectionDegree = "wind_direction_degree"
        case icon                = "code_day"
        case text                = "text_day"
    }

    var id: Int? = nil
    var temperature: String? = nil
    var date: String? = nil
    var week: String? = nil
    var bigIcon: String? = nil
    var cloud: String? = nil
    var city: String? = nil

    var min: String? = nil
    var max: String? = nil
    var windDirection: String? = nil
    var windDirectionDegree: String? = nil
    var icon: String? = nil
    var text: String? = nil

}

