//
//  MVideoCell.swift
//  News
//
//  Created by RockerHX on 2018/3/15.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import Kingfisher
import Devices


class MVideoCell: UITableViewCell {

    // MARK: - IBOutlet Property -
    @IBOutlet var coverView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var durationLabel: UILabel?
    @IBOutlet var summaryLabel: UILabel?
    @IBOutlet var playButton: UIImageView?

    @IBOutlet var fakeCountLabel: UILabel?
    @IBOutlet var favoriteCountLabel: UILabel?

    @IBOutlet var durationContainer: UIView?
    @IBOutlet var countContainer: UIStackView?
    
    // MARK: - Public Property -
    // MARK: - Private Property -

    // MARK: - Override Methods -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coverView?.kf.indicatorType = .activity
    }

    // MARK: - Public Methods -
    public func dispay(withVideo video: MVideo) {
        if let coverURL = video.coverURL {
            coverView?.kf.setImage(with: URL(string: coverURL))
        }
        titleLabel?.text = video.title
        durationLabel?.text = video.duration
        summaryLabel?.text = video.clickPrompt ?? ""
        summaryLabel?.text = video.prompt

        fakeCountLabel?.text = video.praise
        favoriteCountLabel?.text = video.collect

        if ((video.mode ?? .native) != .native) {
            playButton?.isHidden = true
            durationContainer?.isHidden = true
            countContainer?.isHidden = true
        }
    }

    // MARK: - Private Methods -

}

