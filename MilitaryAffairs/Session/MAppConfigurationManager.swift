//
//  MAppConfigurationManager.swift
//  News
//
//  Created by RockerHX on 2018/3/20.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import PromiseKit
import Devices


extension MAppConfigurationManager {
    static let AppID = "9"
}


class MAppConfigurationManager {

    // MARK: - Singleton
    static let manager = MAppConfigurationManager()

    // MARK: - Public Property
    public private(set) var configuration: MAppConfiguration?
    public var auditing: Bool {
        guard let version = configuration?.version else {
            return false
        }
        return (version == Device.Application.shared.version)
    }

    // MARK: - Public Property
    private let service = MEntryService()

    fileprivate init() {
        configuration = MAppConfiguration.unArchive() as? MAppConfiguration
        if nil == configuration {
            configuration = try? json.decoded() as MAppConfiguration
        }
        fetchAppCongiure()
    }

    private func fetchAppCongiure() {
        service.fetchAppCongiure(withID: MAppConfigurationManager.AppID)
            .done { [weak self] appConfiguration in
                appConfiguration.cache()
                self?.configuration = appConfiguration
                NotificationCenter.default.post(name: .ConfigureOperationNotification, object: nil)
            }.catch { error in
                debugPrint(error)
            }
    }

}


fileprivate let json = """
{
        "id": 9,
        "name": "军事头条",
        "status": "NORMAL",
        "logo": null,
        "color": null,
        "created_at": null,
        "updated_at": null,
        "version": "",
        "app_key": "5b3a11d9f43e4802bb000261",
        "app_secret": "gkez4amhdhvnca07o0jfbh9d1qgunfbh",
        "loc": 0,
        "loc_name": "本地资讯",
        "channels": [
            {
                "id": 7,
                "name": "军事",
                "status": "NORMAL",
                "code": "junshi",
                "created_at": "2018-03-03T16:14:11.000+0000",
                "updated_at": "2018-03-03T16:14:11.000+0000"
            },
            {
                "id": 87,
                "name": "武器",
                "status": "NORMAL",
                "code": "wuqi",
                "created_at": "2018-07-02T02:24:09.000+0000",
                "updated_at": "2018-07-02T02:24:09.000+0000"
            },
            {
                "id": 88,
                "name": "中国军事",
                "status": "NORMAL",
                "code": "zhongguojunshi",
                "created_at": "2018-07-02T02:24:09.000+0000",
                "updated_at": "2018-07-02T02:24:09.000+0000"
            },
            {
                "id": 89,
                "name": "国际军事",
                "status": "NORMAL",
                "code": "guojijunshi",
                "created_at": "2018-07-02T02:24:09.000+0000",
                "updated_at": "2018-07-02T02:24:09.000+0000"
            },
            {
                "id": 90,
                "name": "军事冲突",
                "status": "NORMAL",
                "code": "junshichongtu",
                "created_at": "2018-07-02T02:24:09.000+0000",
                "updated_at": "2018-07-02T02:24:09.000+0000"
            },
            {
                "id": 91,
                "name": "阅兵",
                "status": "NORMAL",
                "code": "yuebing",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 92,
                "name": "美国军事",
                "status": "NORMAL",
                "code": "meiguojunshi",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 93,
                "name": "中日关系",
                "status": "NORMAL",
                "code": "zhongriguanxi",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 94,
                "name": "军史",
                "status": "NORMAL",
                "code": "junshi",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 95,
                "name": "军事评论",
                "status": "NORMAL",
                "code": "junshipinglun",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 96,
                "name": "中印关系",
                "status": "NORMAL",
                "code": "zhongyinguanxi",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 97,
                "name": "南海",
                "status": "NORMAL",
                "code": "nanhai",
                "created_at": "2018-07-02T02:24:10.000+0000",
                "updated_at": "2018-07-02T02:24:10.000+0000"
            },
            {
                "id": 98,
                "name": "军演",
                "status": "NORMAL",
                "code": "junyan",
                "created_at": "2018-07-02T02:24:11.000+0000",
                "updated_at": "2018-07-02T02:24:11.000+0000"
            },
            {
                "id": 99,
                "name": "空军",
                "status": "NORMAL",
                "code": "kongjun",
                "created_at": "2018-07-02T02:24:11.000+0000",
                "updated_at": "2018-07-02T02:24:11.000+0000"
            },
            {
                "id": 100,
                "name": "陆军",
                "status": "NORMAL",
                "code": "lujun",
                "created_at": "2018-07-02T02:24:11.000+0000",
                "updated_at": "2018-07-02T02:24:11.000+0000"
            },
            {
                "id": 101,
                "name": "海军",
                "status": "NORMAL",
                "code": "haijun",
                "created_at": "2018-07-02T02:24:11.000+0000",
                "updated_at": "2018-07-02T02:24:11.000+0000"
            },
            {
                "id": 102,
                "name": "冷兵器",
                "status": "NORMAL",
                "code": "lengbingqi",
                "created_at": "2018-07-02T02:24:12.000+0000",
                "updated_at": "2018-07-02T02:24:12.000+0000"
            },
            {
                "id": 103,
                "name": "ISIS",
                "status": "NORMAL",
                "code": "isis",
                "created_at": "2018-07-02T02:24:12.000+0000",
                "updated_at": "2018-07-02T02:24:12.000+0000"
            },
            {
                "id": 104,
                "name": "航母",
                "status": "NORMAL",
                "code": "hangmu",
                "created_at": "2018-07-02T02:24:12.000+0000",
                "updated_at": "2018-07-02T02:24:12.000+0000"
            },
            {
                "id": 105,
                "name": "反恐",
                "status": "NORMAL",
                "code": "fankong",
                "created_at": "2018-07-02T02:24:12.000+0000",
                "updated_at": "2018-07-02T02:24:12.000+0000"
            }
        ],
        "videoChannels": [
            {
                "id": 17,
                "name": "娱乐",
                "status": "NORMAL",
                "code": "yule",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 18,
                "name": "社会",
                "status": "NORMAL",
                "code": "shehui",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 19,
                "name": "美食",
                "status": "NORMAL",
                "code": "meishi",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 20,
                "name": "搞笑",
                "status": "STOP",
                "code": "gaoxiao",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 21,
                "name": "世界",
                "status": "NORMAL",
                "code": "shijie",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 22,
                "name": "科技",
                "status": "NORMAL",
                "code": "keji",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 23,
                "name": "体育",
                "status": "STOP",
                "code": "tiyu",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 24,
                "name": "财富",
                "status": "NORMAL",
                "code": "caifu",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 25,
                "name": "汽车",
                "status": "NORMAL",
                "code": "qiche",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 26,
                "name": "新知",
                "status": "NORMAL",
                "code": "xinzhi",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 27,
                "name": "生活",
                "status": "NORMAL",
                "code": "shenghuo",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 28,
                "name": "二次元",
                "status": "STOP",
                "code": "erciyuan",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            },
            {
                "id": 29,
                "name": "拍客",
                "status": "STOP",
                "code": "paike",
                "created_at": "2018-03-09T02:31:43.000+0000",
                "updated_at": "2018-03-09T02:31:43.000+0000"
            }
        ],
        "spaces": [
            {
                "id": 7,
                "name": "穿山甲",
                "code": null,
                "appid": "5159140",
                "status": "NORMAL",
                "adposes": [
                    {
                        "id": 46,
                        "name": "开屏广告",
                        "posid": "887464595",
                        "type": "SCREEN",
                        "adposList": []
                    },
                    {
                        "id": 47,
                        "name": "原生广告",
                        "posid": "10005",
                        "type": "NATIVE",
                        "adposList": [
                            {
                                "id": 48,
                                "name": "上文下图",
                                "posid": "946039748",
                                "mode": "DOWNIMG"
                            },
                            {
                                "id": 49,
                                "name": "纯图1280",
                                "posid": "946039731",
                                "mode": "IMG1280"
                            },
                            {
                                "id": 50,
                                "name": "左图右文",
                                "posid": "946039740",
                                "mode": "LEFTIMG"
                            }
                        ]
                    }
                ]
            }
        ]
    }
""".data(using: .utf8)!

