//
//  NMainViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/27.
//Copyright © 2018年 RockerHX. All rights reserved.
//
//  GitHub: https://github.com/rockerhx
//


import UIKit


enum NMainSegue: String, HXSegue {
    case Main2Channel  = "Main2Channel"
}


class NMainViewController: UITabBarController {

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    // MARK: - Private Property -
    private var splashAD: GDTSplashAd?

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !LaunchManager.manager.channelsComfirmed {
            performSegue(withIdentifier: NMainSegue.Main2Channel.identifier, sender: nil)
        }
    }

    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if NMainSegue.Main2Channel.identifier == identifier {
                let channelViewController = (segue.destination as? UINavigationController)?.viewControllers.first as? NChannelViewController
                channelViewController?.delegate = self
            }
        }
    }

    // MARK: - Event Methods -
    // MARK: - Public Methods -
    // MARK: - Private Methods -
    private func configure() {
        if LaunchManager.manager.channelsComfirmed {
            showSplashAD()
        }
    }
    
}


// MARK: - NChannelViewControllerDelegate -
extension NMainViewController: NChannelViewControllerDelegate {

    func channelsComfirm() {
        let principalViewController = (viewControllers?.first as? UINavigationController)?.viewControllers.first as? NPrincipalViewController
        principalViewController?.channelsReload()
    }
}


extension NMainViewController: GDTSplashAdDelegate {

    private func showSplashAD() {
        GDTSDKConfig.setHttpsOn()
        splashAD = GDTSplashAd(appId: "1105344611", placementId: "9040714184494018")
        splashAD?.delegate = self
        splashAD?.fetchDelay = 5
        splashAD?.loadAndShow(in: (UIApplication.shared.delegate?.window)!)
    }

    func splashAdSuccessPresentScreen(_ splashAd: GDTSplashAd!) {
        print("splashAdSuccessPresentScreen")
    }

    func splashAdClosed(_ splashAd: GDTSplashAd!) {
//        showMainSence()
        print("splashAdClosed")
    }


    func splashAdFail(toPresent splashAd: GDTSplashAd!, withError error: Error!) {
        print("error: \(error)")
    }

    func splashAdLifeTime(_ time: UInt) {
        print("time: \(time)")
    }
}

