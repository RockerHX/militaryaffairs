//
//  MChannelViewController.swift
//  News
//
//  Created by RockerHX on 2018/2/28.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import UIKit
import RealmSwift


protocol MChannelViewControllerDelegate {
    func channelsComfirm()
}


class MChannelViewController: UICollectionViewController {

    enum `Type` {
        case news
        case video
    }

    // MARK: - IBOutlet Property -
    // MARK: - Public Property -
    public var delegate: MChannelViewControllerDelegate?
    public var type = Type.news

    // MARK: - Private Property -
    fileprivate var channels = [MDChannel]()

    // MARK: - View Controller Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }

    /*
    // MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Event Methods -
    @IBAction func comfirmButtonTap() {
        if selectedChannels().isEmpty {
            showHud(with: "请至少选择一个频道!!!")
            collectionView(collectionView!, didSelectItemAt:
                IndexPath(item: 0, section: 0))
        } else {
            LaunchManager.manager.channelscomfirm()
            delegate?.channelsComfirm()
            dismiss(animated: true, completion: nil)
        }
    }

    // MARK: - Private Methods -
    private func configure() {
        fetchChannels()
    }

    fileprivate func fetchChannels() {
        let cacheChannels = (type == .news) ? MDChannel.fetchDressChannels() : MDChannel.fetchVideoChannels()
        if let fetchedChannels = cacheChannels {
            channels = fetchedChannels
            collectionView?.reloadData()
        }
    }

    fileprivate func selectedChannels() -> [MDChannel] {
        return channels.filter({ $0.select })
    }

}


// MARK: - Collection View Data Source Methods -
extension MChannelViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return channels.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MChannelCell.self), for: indexPath) as! MChannelCell

        // Configure the cell
        cell.display(withChannel: channels[indexPath.row])

        return cell
    }
}


// MARK: - Collection View Delegate Methods -
extension MChannelViewController {

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if 0 == indexPath.row {
            showHud(with: "默认频道不可取消！！！")
            collectionView.reloadData()
        } else {
            let channel = channels[indexPath.row]
            _ = channel.changeSelectState()
        }
    }
}

