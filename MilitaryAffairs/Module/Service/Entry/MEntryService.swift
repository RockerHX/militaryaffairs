//
//  MEntryService.swift
//  News
//
//  Created by RockerHX on 2018/3/14.
//  Copyright © 2018年 RockerHX. All rights reserved.
//


import Moya
import RxSwift
import SwiftyJSON
import PromiseKit


struct MEntryService {

    private let disposeBag = DisposeBag()
    
}


extension MEntryService {

    public func fetchAppCongiure(withID id: String) -> Promise<MAppConfiguration> {
        return Promise { result in
            let provider = MoyaProvider<MApi>()
            provider.rx.request(.app(id: id))
                .filterSuccessfulStatusCodes()
                .mapAppConfigure()
                .subscribe({ (event) in
                    switch event {
                    case .next(let appConfiguration):
                        result.fulfill(appConfiguration)
                    case .error(let error):
                        result.reject(error)
                    case .completed:
                        return
                    }
                }).disposed(by: disposeBag)
        }
    }

}


extension PrimitiveSequence where Trait == SingleTrait, Element == Response {

    fileprivate func mapAppConfigure() -> Observable<MAppConfiguration> {
        return flatMap { response -> Single<MAppConfiguration> in
            return Single.just(try response.mapAppConfigure())
        }.asObservable()
    }

}


extension Response {

    fileprivate func mapAppConfigure() throws -> MAppConfiguration {
        let jsonMapError = MoyaError.jsonMapping(self)
        if let json = try? JSON(data: data),
            let code = json["code"].int,
            let tip = json["tip"].string {
            if 0 == code {
                do {
                    let appConfiguration = try JSONDecoder().decode(MAppConfiguration.self, from: json["data"].rawData())
                    return appConfiguration
                } catch {
                    throw error
                }
            } else {
                throw MoyaError.requestMapping(tip)
            }
        }
        throw jsonMapError
    }

}

